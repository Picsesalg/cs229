/**
 * Alice Yang
 * ayang36@jhu.edu
 * param.cpp
 */

#include<iostream>
#include<string>
#include<sstream>
#include<cstdlib>
#include<iomanip>

using std::cin;
using std::cout;
using std::string;
using std::istringstream;
using std::hex;
using std::setprecision;
using std::fixed;

int main() {

	long num_tot = 0;
	long num_for = 0;
	long num_bac = 0;
	long dist_for = 0;
	long dist_bac = 0;

	string line = "";
	long add_pc = 0;
	long add_tg = 0;
	char taken = 'a';

	string word = "";

	while (getline(cin, line)) {
		int flag = 1;
		for (unsigned int i = 0; i < line.length(); i++) {
			if (!isspace(line[i])) {
				flag = 0;
				break;
			}
		}
		if (flag) {
			continue;
		}
		num_tot++;
		istringstream iss(line);
		iss >> hex >> add_pc;
		iss >> hex >> add_tg;
		iss >> taken;
		if (add_tg >= add_pc) { /* Forward pass. */
			num_for++;
			dist_for += add_tg - add_pc;
		} else { /* Backward pass. */
			num_bac++;
			dist_bac += add_pc - add_tg;
		}
	}

	if (num_tot == 0) {
		num_for = 0;
		num_bac = 0;
	}

	cout << "Total " << num_tot << "\n";
	cout << "Forward " << num_for << "\n";
	cout << "Backward " << num_bac << "\n";

	double frac = num_for * 1.0 / num_tot;
	
	if (num_tot == 0) {
		frac = 0.0;
	}

	frac = frac * 100;

	cout << fixed;
	cout << "Forward% " << setprecision(2) <<frac << "\n";
	
	frac = num_bac * 1.0 / num_tot;

	if (num_tot == 0) {
		frac = 0.0;
	}

	frac = frac * 100;

	cout << "Backward% " << setprecision(2) << frac << "\n";

	frac = dist_for * 1.0 / num_for;

	if (num_for == 0) {
		frac = 0.0;
	}

	cout << "Forward-distance " << setprecision(2) << frac << "\n";

	frac = dist_bac * 1.0 / num_bac;
	
	if (num_bac == 0) {
		frac = 0.0;
	}

	cout << "Backward-distance " << setprecision(2) << frac << "\n";
	
	return 0;
}
