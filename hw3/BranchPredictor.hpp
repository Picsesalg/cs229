/**
 * Alice Yang
 * ayang36@jhu.edu
 * BranchPredictor.hpp
 */

#ifndef ALWAYSTAKEN_H
#define ALWAYSTAKEN_H

#include<vector>

/**
 * Generic subclass for all branch predictors.
 */
class BranchPredictor {

public:
	BranchPredictor () {
		tot_branch = 0;
		tot_good = 0;
		tot_bad = 0;
		size = 0;
		start = 0;
		end = 0;
		taken = 'a';
	}

	/* Increments the number of branches. */
	void inc_tot();

	/* Sets the starting PC address for each line. */
	void set_start(std::string input);

	/* Sets the branch target address for each line. */
	void set_end(std::string input);

	/* Sets the value of taken on each line. */
	void set_take(char input);

	/* Print the output. */
	void toString();

protected:
	/* Total number of branches. */
	long tot_branch;
	/* Total number of good predictions. */
	long tot_good;
	/* Total number of bad predictions. */
	long tot_bad;
	/* Size of the state of branch predictor. */
	long size;
	/* PC address. */
	long start;
	/* Branch target address. */
	long end;
	/* The state of the taken token. */
	char taken;
};

/**
 * Class for Always Taken predicitions
 */
class AT : public BranchPredictor {

public:
	AT() 
	: BranchPredictor() {}

	/* Increments the accuracy of always taken predictions. */
	void predict();
	
};

/**
 * Class for Never Taken predictions.
 */
class NT : public BranchPredictor {

public:
	NT()
	: BranchPredictor() {}

	/* Increments the accuracy of never taken predictions. */
	void predict();
};

/**
 * Class for the backwards taken, forward not heuristic.
 */
class BTFN : public BranchPredictor {

public:
	BTFN()
	: BranchPredictor() {}

	/* Increment good if backwards and taken, or forwards and not taken. */
	void predict(std::string add_pc, std::string add_tg);
};

class Bimodal : public BranchPredictor {

public:
	Bimodal()
	: BranchPredictor() {
		slots = 0;
		slots_bits = 0;
		steps = 0;
		steps_bits = 0;
	}

	/* Set slot size. */
	void set_slot(std::string input);

	/* Set step size. */
	void set_step(std::string input);

	/* Calculates size. */
	void set_size();

	/* Checks the prediction table for counter. */
	void check_table();

	/* Checks if the prediction is correct. */
	void predict(int predn);

private:
	int slots;
	int slots_bits;
	int steps;
	int steps_bits;
	/* Contains sat. counters at each slot. */
	std::vector<int> predict_table;
};

/**
 * Class and object for the two level global dynamic predictor.
 */
class TwoLevelGlobal : public BranchPredictor {

public:
	TwoLevelGlobal()
	: BranchPredictor() {
		slots = 0;
		slots_bits = 0;
		hist = 0;
		hist_bits = 0;
		steps = 0;
		steps_bits = 0;
	}

	/* Sets the slot, step, and historypattern sizes. */
	void set_params(std::string slot, std::string his, std::string steps);

	/* Calculate the size of the TwoLevel*/
	void set_size();

	/* Check the history pattern at the specified slot. */
	void check_hist();

	/* See if the prediction in the prediction table is accurate. */
	void predict(int pattern);

private:
	int slots;
	int slots_bits;
	int hist;
	int hist_bits;
	int steps;
	int steps_bits;
	/* Contains slot amount of history patterns. */
	std::vector<int> history_table;
	/* Contains the counters for each history pattern. */
	std::vector<int> predict_table;
};

class TwoLevelLocal : public BranchPredictor {

public:
	TwoLevelLocal()
	: BranchPredictor() {
		slots = 0;
		slots_bits = 0;
		hist = 0;
		hist_bits = 0;
		steps = 0;
		steps_bits = 0;
	}

	void set_params(std::string slot, std::string his, std::string step);

	void set_size();

	void check_hist();

	void predict(int address);

private:
	int slots;
	int slots_bits;
	int hist;
	int hist_bits;
	int steps;
	int steps_bits;
	/* Slots amount of history pattern. */
	std::vector<int> history_table;
	/* For each slot, there's historypattern amount of counters. */
	std::vector<int> predict_table;
};

#endif
