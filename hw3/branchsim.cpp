/**
 * Alice Yang
 * ayang36@jhu.edu
 * branchsim.cpp
 */

#include<iostream>
#include<string>
#include<sstream>
#include<cstdlib>
#include<iomanip>

#include"BranchPredictor.hpp"

using std::cerr;
using std::string;
using std::cin;
using std::istringstream;
using std::stoi;
using std::endl;

/* Check if the slots or history pattern arguments are valid. */
int valid_slot(int input);
/* Check if the steps argument is valid. */
int valid_step(int input);
/* Handles Always Taken. */
void at();
/* Handles Never Taken. */
void nt();
/* Handles Back taken Froward never. */
void btfn();
/* Handles bimodal. */
void bimodal(char *argv[]);
/* Handles two level global. */
void global(char *argv[]);
/* Handles two level local. */
void local(char *argv[]);

int main(int args, char *argv[]) {

	switch(args) {
	case(2):
		if (string(argv[1]).compare("at") == 0) {
			at();
		} else if (string(argv[1]).compare("nt") == 0) {
			nt();
		} else if (string(argv[1]).compare("btfn") == 0) {
			btfn();
		} else {
			cerr << "Invalid argument.\n";
			exit(EXIT_FAILURE);
		}
		break;
	case(4):
		if (string(argv[1]).compare("bimodal") != 0) {
			cerr << "Invalid argument.\n";
			exit(EXIT_FAILURE);
		} else if (valid_slot(stoi(argv[2])) == 0) {
			cerr << "Invalid argument.\n";
			exit(EXIT_FAILURE);
		} else if (valid_step(stoi(argv[3])) == 0) {
			cerr << "Invalid argument.\n";
			exit(EXIT_FAILURE);
		} else {
			bimodal(argv);
		}
		break;
	case(6):
		if (string(argv[1]).compare("twolevel") != 0) {
			cerr << "Invalid argument.\n";
			exit(EXIT_FAILURE);
		} else if (valid_slot(stoi(argv[2])) == 0) {
			cerr << "Invalid argument.\n";
			exit(EXIT_FAILURE);
		} else if (valid_slot(stoi(argv[3])) == 0) {
			cerr << "Invalid argument.\n";
			exit(EXIT_FAILURE);
		} else if (!(string(argv[4]).compare("global") == 0 ||
			string(argv[4]).compare("local") == 0)) {
			cerr << "Invalid argument.\n";
			exit(EXIT_FAILURE);
		} else if (valid_step(stoi(argv[5])) == 0) {
			cerr << "Invalid argument5.\n";
			exit(EXIT_FAILURE);
		} else {
			if (string(argv[4]).compare("global") == 0) {
				global(argv);
			} else if (string(argv[4]).compare("local") == 0) {
				local(argv);
			}
		}
		break;
	default:
		cerr << "Invalid number of arguments.\n";
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}

int valid_slot(int input) {
	/* Ensure the slot is in the range. */
	if (input < 4 || input > 65536) {
		return 0;
	}
	/* Ensures slots is a power of 2. */
	if ((input & (input - 1)) == 0) {
		return 1;
	} else {
		return 0;
	}
}

int valid_step(int input) {
	/* Ensures range. */
	if (input < 2 || input > 16) {
		return 0;
	}
	/* Ensures power of 2. */
	if ((input & (input - 1)) == 0) {
		return 1;
	} else {
		return 0;
	}
}

void at() {
	string line = "";
	string add_pc = "";
	string add_tg = "";
	char token = 'a';
	
	AT at = AT();
        while (getline(cin, line)) {
		istringstream iss(line);
		int flag = 1;
		for (unsigned int i = 0; i < line.length(); i++) {
			if (!isspace(line[i])) {
				flag = 0;
				break;
			}
		}
		if (flag) {
			continue;
		}
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add_pc;
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add_tg;
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> token;
		at.inc_tot();
		at.set_start(add_pc);
		at.set_end(add_tg);
		at.set_take(token);
		at.predict();
	}
	at.toString();
}

void nt() {
	string line = "";
	string add_pc = "";
	string add_tg = "";
	char token = 'a';
	
	NT nt = NT();
	
	while (getline(cin, line)) {
		istringstream iss(line);
		int flag = 1;
                for (unsigned int i = 0; i < line.length(); i++) {
                        if (!isspace(line[i])) {
                                flag = 0;
                                break;
                        }
                }
                if (flag) {
                        continue;
                }
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add_pc;
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add_tg;
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> token;
		nt.inc_tot();
		nt.set_start(add_pc);
		nt.set_end(add_tg);
		nt.set_take(token);
		nt.predict();
	}
	nt.toString();
}

void btfn() {
	string line = "";
	string add_pc = "";
	string add_tg = "";
	char token = 'a';
	
	BTFN btfn = BTFN();
	
	while (getline(cin, line)) {
		istringstream iss(line);
		int flag = 1;
                for (unsigned int i = 0; i < line.length(); i++) {
                        if (!isspace(line[i])) {
                                flag = 0;
                                break;
                        }
                }
                if (flag) {
                        continue;
                }
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add_pc;
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add_tg;
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> token;
		btfn.inc_tot();
		btfn.set_start(add_pc);
		btfn.set_end(add_tg);
		btfn.set_take(token);
		btfn.predict(add_pc, add_tg);
	}
	btfn.toString();
}

void bimodal(char *argv[]) {
	string line = "";
	string add_pc = "";
	string add_tg = "";
	char token = 'a';
	
	Bimodal bimodal = Bimodal();
	
	bimodal.set_slot(string(argv[2]));
	bimodal.set_step(string(argv[3]));
	bimodal.set_size();
	while (getline(cin, line)) {
		istringstream iss(line);
		int flag = 1;
                for (unsigned int i = 0; i < line.length(); i++) {
                        if (!isspace(line[i])) {
                                flag = 0;
                                break;
                        }
                }
                if (flag) {
                        continue;
                }
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add_pc;
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add_tg;
		if (iss.eof()) {
			cerr << "Incorrect formatted line.\n";
			exit(EXIT_FAILURE);
		}
		iss >> token;
		bimodal.inc_tot();
		bimodal.set_start(add_pc);
		bimodal.set_end(add_tg);
		bimodal.set_take(token);
		bimodal.check_table();
	}
	bimodal.toString();
}

void global(char *argv[]) {

	string line = "";
	string add_pc = "";
	string add_tg = "";
	char token = 'a';

	TwoLevelGlobal glob = TwoLevelGlobal();
	
	glob.set_params(string(argv[2]), string(argv[3]), string(argv[5]));
	glob.set_size();
	while (getline(cin, line)) {
		istringstream iss(line);
		int flag = 1;
                for (unsigned int i = 0; i < line.length(); i++) {
                        if (!isspace(line[i])) {
                                flag = 0;
                                break;
                        }
                }
                if (flag) {
                        continue;
                }
		if (iss.eof()) {
                	cerr << "Incorrect formatted line.\n";
                        exit(EXIT_FAILURE);
                }
		iss >> add_pc;
		if (iss.eof()) {
                        cerr << "Incorrect formatted line.\n";
                        exit(EXIT_FAILURE);
                }
		iss >> add_tg;
		if (iss.eof()) {
                        cerr << "Incorrect formatted line.\n";
                        exit(EXIT_FAILURE);
                }
		iss >> token;
		glob.inc_tot();
		glob.set_start(add_pc);
		glob.set_end(add_tg);
		glob.set_take(token);
		glob.check_hist();
	}
	glob.toString();
}

void local(char *argv[]) {

	string line = ""; 
        string add_pc = ""; 
        string add_tg = ""; 
        char token = 'a';

	TwoLevelLocal loc = TwoLevelLocal();

	loc.set_params(argv[2], argv[3], argv[5]);
	loc.set_size();

	while (getline(cin, line)) {
		istringstream iss(line);
		int flag = 1;
                for (unsigned int i = 0; i < line.length(); i++) {
                        if (!isspace(line[i])) {
                                flag = 0;
                                break;
                        }
                }
                if (flag) {
                        continue;
                }
		if (iss.eof()) {
                        cerr << "Incorrect formatted line.\n";
                        exit(EXIT_FAILURE);
                }
		iss >> add_pc;
		if (iss.eof()) {
                        cerr << "Incorrect formatted line.\n";
                        exit(EXIT_FAILURE);
                }
		iss >> add_tg;
		if (iss.eof()) {
                        cerr << "Incorrect formatted line.\n";
                        exit(EXIT_FAILURE);
                }
		iss >> token;
		loc.inc_tot();
		loc.set_start(add_pc);
		loc.set_end(add_tg);
		loc.set_take(token);
		loc.check_hist();
	}
	loc.toString();
}
