/**
 * Alice Yang
 * ayang36@jhu.edu
 * BranchPredictor.cpp
 */

#include<iostream>
#include<iomanip>
#include<cmath>
#include<vector>
#include<string>
#include<sstream>
#include"BranchPredictor.hpp"

using std::cout;
using std::endl;
using std::setprecision;
using std::fixed;
using std::cerr;
using std::log2;
using std::vector;
using std::string;
using std::stringstream;
using std::hex;

void BranchPredictor::inc_tot() {
	tot_branch++;
}

void BranchPredictor::set_start(string input) {
	stringstream ss;

	for (unsigned int i = 0; i < input.length(); i++) {
		if (!isxdigit(input[i])) {
			cerr << "Address is invalid.\n";
			exit(EXIT_FAILURE);
		}
	}

	ss << hex << input;
	ss >> start;
}

void BranchPredictor::set_end(string input) {
	stringstream ss;

	for (unsigned int i = 0; i < input.length(); i++) {
                if (!isxdigit(input[i])) {
                        cerr << "Address is invalid.\n";
                        exit(EXIT_FAILURE);
                }
        }

	ss << hex << input;
	ss >> end;
}

void BranchPredictor::set_take(char input) {
	if (!(input == 'T' || input == 'N')) {
		cerr << "Invalid taken.\n";
		exit(EXIT_FAILURE);
	}
	taken = input;
}

void BranchPredictor::toString() {
	
	double good_perc = tot_good * 1.0 / tot_branch;
	good_perc *= 100;
	double bad_perc = tot_bad * 1.0 / tot_branch;
	bad_perc *= 100;

	if (tot_branch == 0) {
		tot_good = 0;
		tot_bad = 0;
		good_perc = 0.0;
		bad_perc = 0.0;
	}

	cout << "Total " << tot_branch << endl;
	cout << "Good " << tot_good << endl;
	cout << "Bad " << tot_bad << endl;
	cout << fixed;
	cout << "Good% " << setprecision(2) << good_perc << endl;
	cout << "Bad% " << setprecision(2) << bad_perc << endl;
	cout << "Size " << size << endl;
}
void AT::predict() {
	if (taken == 'T') {
		tot_good++;
	} else {
		tot_bad++;
	}
}

void NT::predict() {
	if (taken == 'N') {
		tot_good++;
	} else {
		tot_bad++;
	}
}

void BTFN::predict(string add_pc, string add_tg) {
	
	stringstream ss;
	int start;
	int end;

	ss << hex << add_pc;
	ss >> start;

	stringstream ss2;

	ss2 << hex << add_tg;
	ss2 >> end;

	/* Backwards branch. */
	if (start > end) {
		if (taken == 'T') {
			tot_good++;
		} else {
			tot_bad++;
		}
	} else { /* Forwards. */
		if (taken == 'N') {
			tot_good++;
		} else {
			tot_bad++;
		}
	}
}

void Bimodal::set_slot(string input) {
	slots = stoi(input);
	slots_bits = log2(slots);
	predict_table = vector<int>(slots, 0);
}

void Bimodal::set_step(string input) {
	steps = stoi(input);
	steps_bits = log2(steps);
}

void Bimodal::set_size() {
	size = slots * steps_bits;
}

void Bimodal::check_table() {
	int add = start & (slots - 1);
	int predn = predict_table.at(add);
	Bimodal::predict(predn);
	if (taken == 'T' && predn != steps - 1) {
		predn++;
		predict_table.at(add) = predn;
	} else if (taken == 'N' && predn != 0) {
		predn--;
		predict_table.at(add) = predn;
	}
}

void Bimodal::predict(int predn) {
	if (taken == 'T' && predn >= steps / 2) {
		tot_good++;
	} else if (taken == 'N' && predn < steps / 2) {
		tot_good++;
	} else {
		tot_bad++;
	}
}

void TwoLevelGlobal::set_params(string slot, string his, string step) {
	/* Establishing slots. */
	slots = stoi(slot);
	slots_bits = log2(slots);
	history_table = vector<int>(slots, 0);

	/* Establishing history patterns. */
	hist = stoi(his);
	hist_bits = log2(hist);
	predict_table = vector<int>(hist, 0);

	/* Establishing steps. */
	steps = stoi(step);
	steps_bits = log2(steps);
}

void TwoLevelGlobal::set_size() {
	size = slots * hist_bits + hist * steps_bits;
}

void TwoLevelGlobal::check_hist() {
	int hist_add = start & (slots - 1);
	int pred_add = history_table.at(hist_add) & (hist - 1);
	TwoLevelGlobal::predict(pred_add);
	pred_add = pred_add << 1;
	if (taken == 'T') {
		pred_add++;
	}
	history_table.at(hist_add) = pred_add;
}

void TwoLevelGlobal::predict(int pattern) {
	int predn = predict_table.at(pattern);
	
	if (taken == 'T' && predn >= steps / 2) {
		tot_good++;
	} else if (taken == 'N' && predn < steps / 2) {
		tot_good++;
	} else {
		tot_bad++;	
	}

	/* Update the prediction table. */
	if (taken == 'T' && predn != steps - 1) {
		predn++;
		predict_table.at(pattern) = predn;
	} else if (taken == 'N' && predn != 0) {
		predn--;
		predict_table.at(pattern) = predn;
	}
}

void TwoLevelLocal::set_params(string slot, string his, string step) {
	slots = stoi(slot);
	slots_bits = log2(slots);
	history_table = vector<int>(slots, 0);

	hist = stoi(his);
	hist_bits = log2(hist);
	predict_table = vector<int>(slots * hist, 0);

	steps = stoi(step);
	steps_bits = log2(steps);
}

void TwoLevelLocal::set_size() {
	size = slots * hist_bits + slots * hist * steps_bits;
}

void TwoLevelLocal::check_hist() {
	int pred_add = start & (slots - 1);
	int hist_pat = history_table.at(pred_add) & (hist - 1);
	int hist_add = pred_add * hist + hist_pat;
	TwoLevelLocal::predict(hist_add);
	hist_pat = hist_pat << 1;
	if (taken == 'T') {
		hist_pat++;
	}
	history_table.at(pred_add) = hist_pat;
}

void TwoLevelLocal::predict(int address) {
	int predn = predict_table.at(address);

	if (taken == 'T' && predn >= steps / 2) {
		tot_good++;
	} else if (taken == 'N' && predn < steps / 2) {
		tot_good++;
	} else {
		tot_bad++;
	}

	if (taken == 'T' && predn != steps - 1) {
		predn++;
		predict_table.at(address) = predn;
	} else if (taken == 'N' && predn != 0) {
		predn--;
		predict_table.at(address) = predn;
	}
}
