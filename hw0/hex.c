/**
 * hex.c
 * Alice Yang
 * ayang36@jhu.edu
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define NUM_BYTES 16

/*
 * Read up to NUM_BYTES characters from standard in and fill them into the
 * given buffer in order. Return how many bytes were actually read, 0 means
 * end-of-file.
 */
static int getchunk(char buffer[NUM_BYTES]) {
	
	int count = 0;
	//Reads in every character and keeps track of how many bytes have been read.
	for (int i = 0; i < NUM_BYTES; i++) {
		if (feof(stdin)) {
			return count;
		}
		else if (scanf("%c", &buffer[i]) == 1){
			count++;
		}
	}
	return NUM_BYTES;
}

/*
 * Write a line of the hexdump. The first length characters in buffer
 * are valid, and we're currently at the given position.
 */
static void putchunk(char buffer[NUM_BYTES], int length, int position) {

	//Converting the int position to an unsigned char.
	unsigned char position_hex = (unsigned char) position;

	//Keeps track of the position printed
	int print_position = 0;

	//Printing the position of the line to be printed.
	printf("%08x", position_hex);
	printf(": ");
	//Printing the hexadecimal value of each character.
	for (int i = 0; i < length; i++) {
		unsigned char buffer_character = (unsigned char) buffer[i];
		printf("%02x ", buffer_character);
		print_position+=3;
	}
	for (int i = 0; i < (49 - print_position); i++) {
		printf(" ");
	}
	//Printing the actual line in English.
	for (int i = 0; i < length; i++) {
		if (isprint(buffer[i]) == 0) {
			printf(".");
		}
		else {
			printf("%c", buffer[i]);
		}
	}
	printf("\n");
}

int main(void)
{
	int position = 0;

	char buffer[NUM_BYTES];
	int length;

	while ((length = getchunk(buffer)) != 0) {
		putchunk(buffer, length, position);
		position += length;
	}

	return EXIT_SUCCESS;
}
