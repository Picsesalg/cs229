#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#include "opc.h"

#define UNKNOWN "???"

int main() {
	int c; //The character to be read in.
	int i = 0; //Number of chars read in.
	while ((c = getchar()) != EOF && i < 16) {
		uint8_t inst = (c >> 4) & 0xf;
		uint8_t addr = c & 0xf;
		const char *inst_code = opc_decode(inst);
		if (inst_code == NULL) {
			inst_code = UNKNOWN;
		}
		printf("%x: %s %x\n", i, inst_code, addr);
		i++;
	}
	if (!feof(stdin)) {
		fprintf(stderr, "dis: Program too long. Truncated to 16 bytes.\n");
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}
