#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <ctype.h>

#include "opc.h"

#define OPCODES 3 
#define MAX_MEM 16
#define MAX_CHAR 1000

struct Address {
	int own_addr;
	char label[MAX_CHAR];
	char opc[OPCODES + 1];
	char ref_addr[MAX_CHAR];
};

/**
 * Stores the pointers to the labels with the indices representing the address they're at.
 * Stores the opcodes, who have length of 3.
 * Stores the instructions.
 */
struct Scram {
	struct Address memory[MAX_MEM];
};

int search(struct Scram info, char *ref_addr);
int checkDigit(char *string);
char convert(struct Scram info, int index);
void print(struct Scram info);
int determining_data(char* word, int word_num, int entered_line, int cop_code);
struct Address process_line(char *line, int line_num, int entered_line);
struct Scram load_line();

int main() {
	struct Scram info = load_line();
	/**for (int i = 0; i < MAX_MEM; i++) {
		printf("%d| %s: %s %s\n", info.memory[i].own_addr, info.memory[i].label, info.memory[i].opc, info.memory[i].ref_addr);
	}*/
	print(info);
	exit(EXIT_SUCCESS);
}

/**
 * Returns the address of the label if found.
 * If not -1 is retuned.
 */
int search(struct Scram info, char* ref_addr) {
	for (int i = 0; i < MAX_MEM; i++) {
		//printf("rghjk %s: %s\n", ref_addr, info.memory[i].label);
		if (strcmp(ref_addr, info.memory[i].label) == 0) {
			return i;
		}
	}
	return -1;
}

/**
 * Checks if a string is a number.
 * If it is, it'll return the number. 
 * If not, returns -1;
 */
int checkDigit(char *string) {
	int number;
	for (unsigned int i = 0; i < strlen(string); i++) {
		if (!isdigit(string[i])) {
			return -1;
		}
	}
	number = atoi(string);
	return number;
}

char convert(struct Scram info, int index) {
	uint8_t first;
	uint8_t second;
	//uint8_t actual;
	int actual_char;
	int temp;
	int temp2;
       
	//Converts the label into a number.
	temp = opc_encode(info.memory[index].opc);
	first = (temp << 4) & 0xf0;
	/**
	 * For all instructions except DAT.
	 * First see if the address is number.
	 * If not then check if the label exists.
	 */
	if (temp != -1) {
		first = (temp << 4) & 0xf0;
		//The address is just a number
		if (checkDigit(info.memory[index].ref_addr) != -1) {
			temp = checkDigit(info.memory[index].ref_addr);
			second = temp & 0xf;
		}
		//The address is a label
		else {
			temp = search(info, info.memory[index].ref_addr);
			second = temp & 0xf;	
		}
		actual_char = first | second;
	}
	/**
	 * Handling DAT cases now.
	 */
	else {
		//DAT value is a number
		if (checkDigit(info.memory[index].ref_addr) != -1) {
			temp = checkDigit(info.memory[index].ref_addr);
			//printf("gfi: %d", temp);
			actual_char = temp;
		}
		//The DAT value is a label address.
		else {
			temp2 = search(info, info.memory[index].ref_addr);
			first = opc_encode(info.memory[temp2].opc);
			first = (first << 4) & 0xf0;
			
			if (checkDigit(info.memory[temp2].ref_addr) != -1) {
                        	temp = checkDigit(info.memory[temp2].ref_addr);
                        	second = temp & 0xf;
                	}
                	else {
                        	temp = search(info, info.memory[temp2].ref_addr);
                        	second = temp & 0xf;
                	}
			actual_char = first | second;
		}
	}

	//printf("Line: %d first: %d second%d Actual: %c\n", index, first, second, actual_char);

	return actual_char;
}

void print(struct Scram info) {
	char character;

	for (int i = 0; i < MAX_MEM; i++) {
		if (info.memory[i].own_addr == 0 && i == 0) {
			character = convert(info, i);
			putchar(character);
		}
		else if (info.memory[i].own_addr != 0) {
			character = convert(info, i);
			putchar(character);
		}
	}
	//printf("Completed\n");	
}

/**
 * Determines the type of data the token word is
 */
int determining_data(char* word, int word_num, int entered_line, int cop_code) {
	//printf("det word: %s\n", word);
	//It's a comment.
	if (word[0] == '#') {
		return 4;
	}
	//It's a label.
	else if (word[strlen(word) - 1] == ':') {
		if (strlen(word) > 33) {
			fprintf(stderr, "Label length exceeded in line: %d.\n", entered_line);
			exit(EXIT_FAILURE);
		}
		if (cop_code == 1) {
			fprintf(stderr, "2 labels unallowed in line: %d.\n", entered_line);
			exit(EXIT_FAILURE);
		}
		return 1;
	}
	//It's an OPCODE.
	else if (word_num == 1) {
		if (!opc_valid(word)) {
			fprintf(stderr, "Opcode not valid in line: %d.\n", entered_line);
			exit(EXIT_FAILURE);
		}
		return 2;
	}
	//It's a reference address.
	else if (word_num == 2) {
		if (strlen(word) > 32) {
			fprintf(stderr, "A label exceeded 32 characters in line: %d.\n", entered_line);
			exit(EXIT_FAILURE);
		}
		/**if (search(info, word) == -1) {
			fprintf(stderr, "An address label doesn't exist in line: %d.\n", entered_line);
		}*/
		return 3;
	}
	//Shouldn't get here, but just in case something fails.
	else {
		fprintf(stderr, "More things after address in line: %d.\n", entered_line);
		exit(EXIT_FAILURE);
		return 0;
	}
}

/**
 * Takes the line passed in and breaks it down, 
 * placing it into either labels, opc, or inst.
 */
struct Address process_line(char *line, int line_num, int entered_line) {
	//The Address to be returned.
	struct Address *new_line = (struct Address *) malloc(sizeof(struct Address));
	//Making a new copy of line.
	char *line_copy = (char *) malloc(sizeof(char) * (strlen(line) + 1));	
	strcpy(line_copy, line);
	
	char *delimiters = " \n\t\r\v\f";
	char *word;
	//1 signifies an opcode. 2 is a reference address.
	int word_num = 1;
	int op_code = 0;

	//printf("Line: %s\nLine copy: %s\n", line, line_copy);
	//Extract the first word.
	word = strtok(line_copy, delimiters);
	
	//printf("Word1: %s\n", word);
	new_line->own_addr = -1;
		
	if (word == NULL) {
		free(line_copy);
		return *new_line;
	}
	if (word[0] == '#') {
		free(line_copy);
		return *new_line;
	}
	//Cycles through all the words and determine if they're
	//comments, labels, opcodes, or reference addresses.
	while (word != NULL) {
		new_line->own_addr = line_num;
		//printf("word2: %s ", word);
		switch(determining_data(word, word_num, entered_line, op_code)) {
			case 1:
				//printf("case 1\n");
				printf("%s", new_line->label);
				strncpy(new_line->label, word, strlen(word) - 1);
				op_code = 1;
				//printf("%s\n", new_line.label);
				break;
			case 2:
				//printf("case 2\n");
				word_num += 1;
                        	strcpy(new_line->opc, word);
				break;
			case 3:
				//printf("case 3\n");
				/**if(strcmp(new_line.opc, "DAT") == 0 && (checkDigit(word) == -1)) {
					if (checkDigit(word) > 255) {
						fprintf(stderr, "The address exceeds the capacity of DAT storage in line: %d.\n", entered_line);
						exit(EXIT_FAILURE);
					}
				}
				else if (checkDigit(word) > 15) {
					fprintf(stderr, "The address exceeds 4 bits in line: %d.\n", entered_line);
					exit(EXIT_FAILURE);
				}*/
				word_num += 1;
				strcpy(new_line->ref_addr, word);
				break;
			case 4:
				//printf("case 4\n");
				free(line_copy);
				return *new_line;
			default:
				new_line->own_addr = -1;

		}
		word = strtok(NULL, delimiters);
	}
	free(line_copy);
	return *new_line;
}

/**
 * Looks at the entirety of the standard input
 * and breaks it down into individual lines.
 */
struct Scram load_line() {
	//The Scram struct to be returned
	struct Scram info;
	//The line at which a new line will be read into.
	int line_num = 0;
	//The actual line number entered
	int entered_line = 0;
	//Max number to be read into the line
	size_t line_max = 128;
	//The array to store the line in.
	char *line = (char *) malloc(sizeof(char) * (line_max + 1));
	
	for (int i = 0; i < MAX_MEM; i++) {
                printf("%d| %s: %s %s\n", info.memory[i].own_addr, info.memory[i].label, info.memory[i].opc, info.memory[i].ref_addr);
        }
		
	//Safety
	if (!line) {
		fprintf(stderr, "Malloc error in line %d.\n", entered_line);
		free(line);
		exit(EXIT_FAILURE);
	}

	/**
	 * Stores each line into string.
	 * Works on the assumption that each line does not exceed 1000 characters long.
	 */
	while (fgets(line, line_max + 1, stdin) != NULL && line_num < 16) {
		if (strlen(line) >= 128) {
			fprintf(stderr, "Line length exceeded in line: %d.\n", entered_line);
			free(line);
			exit(EXIT_FAILURE);
		}
		/**if (line_num > 15) {
			fprintf(stderr, "")
		}*/
		//printf("loads line: %s\n", line);
		struct Address temp = process_line(line, line_num, entered_line);
		if (temp.own_addr != -1) {
			info.memory[line_num] = temp;
			line_num++;
		}
		entered_line++;
	}
	//If more than 16 addresses are read.
	if (fgets(line, line_max, stdin) != NULL || line[0] != '#') {
		/**fprintf(stderr, "Number of instructions exceeded size of the SCRAM in line: %d.\n", entered_line);
		exit(EXIT_FAILURE);*/
	}
	free(line);
	return info;
}
