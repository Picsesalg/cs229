#ifndef _OPC_H
#define _OPC_H

#define MAX_OPC_LEN 3
#define UNKNOWN "???"
#define SCRAM_SIZE 16

/*
 * The SCRAM opcodes. Except for DAT the values are also the 4-bit
 * encoding the simulator understands; DAT is here for convenience
 * but is handled differently.
 */
enum opcodes {
	HLT,
	LDA, LDI,
	STA, STI,
	ADD, SUB,
	JMP, JMZ,
	DAT,
};

/*
 * Is given string a valid opcode? Note that DAT is considered valid.
 */
int opc_valid(const char *opc);

/*
 * Binary encoding for given opcode. Returns -1 on error (including
 * for DAT which doesn't have an encoding).
 */
int opc_encode(const char *opc);

/*
 * String representation for given opcode. Returns NULL on error
 * (including for DAT which doesn't have an encoding).
 */
const char *opc_decode(int opc);

#endif
