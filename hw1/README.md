# sim - basic SCRAM simulator

- say `make` to build
- say `./sim <loop.scram` to run machine code in `loop.scram`
- say `./sim -m 10 <loop.scram` to run only 10 instructions
