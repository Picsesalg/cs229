#include <assert.h>
#include <string.h>

#include "opc.h"

#define NELEM(a) ((int)(sizeof(a) / sizeof(a[0])))

static char *opcodes[] = {
	[HLT] = "HLT",
	[LDA] = "LDA", [LDI] = "LDI",
	[STA] = "STA", [STI] = "STI",
	[ADD] = "ADD", [SUB] = "SUB",
	[JMP] = "JMP", [JMZ] = "JMZ",
	[DAT] = "DAT",
};

static_assert(NELEM(opcodes) - 1 == DAT, "DAT is not the last opcode");

int opc_valid(const char *opc)
{
	return (opc_encode(opc) >= 0) || (!strncmp("DAT", opc, MAX_OPC_LEN));
}

int opc_encode(const char *opc)
{
	int i;
	for (i = 0; i < NELEM(opcodes) - 1; i++) {
		if (!strncmp(opcodes[i], opc, MAX_OPC_LEN)) {
			return i;
		}
	}
	return -1;
}

const char *opc_decode(int opc)
{
	if (opc < 0 || opc > NELEM(opcodes) - 1) {
		return NULL;
	}
	return opcodes[opc];
}
