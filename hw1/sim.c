#include <getopt.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "opc.h"

static uint32_t max_inst; /* maximum number of instructions we execute */
static uint32_t count; /* number of instructions executed so far */

static uint8_t mem[SCRAM_SIZE]; /* memory of the SCRAM */
static uint8_t size; /* how much of the memory has been loaded */

static uint8_t pc; /* program counter register */
static uint8_t ac; /* accumulator register */

/*
 * Load the SCRAM object file from standard input, setting up mem and size.
 *
 * This will truncate (with an error message) the file to SCRAM_SIZE bytes if
 * necessary.
 */
static void load(void)
{
	uint8_t i = 0;
	int c;
	while ((c = getchar()) != EOF && i < SCRAM_SIZE) {
		mem[i] = c;
		i++; /* fine without & 0xf */
	}
	if (!feof(stdin)) {
		fprintf(stderr, "Program too long, truncated to %d bytes.\n",
				SCRAM_SIZE);
	}
	size = i;
}

/*
 * Print state (registers and memory) of the SCRAM to standard output.
 */
static void dump(void)
{
	printf("pc: %1x ac: %02x | mem: ", pc, ac);
	for (uint8_t i = 0; i < SCRAM_SIZE; i++) {
		printf("%02x ", mem[i]);
	}
}

/*
 * Run the SCRAM.
 *
 * We stop if the program tries to execute an illegal opcode, a HLT
 * instruction, or if we reach the maximum number of instructions we're
 * supposed to run.
 */
static void run(void)
{
	pc = 0;
	ac = 0;
	count = 0;

	while (count < max_inst) {
		dump();

		uint8_t ir = mem[pc];
		uint8_t opc = (ir >> 4) & 0xf;
		uint8_t adr = ir & 0xf;
		uint8_t t;

		const char *mne = opc_decode(opc);
		if (mne == NULL) {
			mne = UNKNOWN;
		}
		printf("| %s %1x\n", mne, adr);

		pc = (pc + 1) & 0xf;

		switch (opc) {
		case HLT:
			printf("HLT encountered @ %1x\n", (pc - 1) & 0xf);
			return;
		case LDA:
			ac = mem[adr];
			break;
		case LDI:
			t = mem[adr] & 0xf;
			ac = mem[t];
			break;
		case STA:
			mem[adr] = ac;
			break;
		case STI:
			t = mem[adr] & 0xf;
			mem[t] = ac;
			break;
		case ADD:
			ac = (ac + mem[adr]) & 0xff;
			break;
		case SUB:
			ac = (ac - mem[adr]) & 0xff;
			break;
		case JMP:
			pc = adr;
			break;
		case JMZ:
			if (ac == 0) {
				pc = adr;
			}
			break;
		default:
			fprintf(stderr,
				"illegal instruction %02x encountered @ %1x\n",
				ir, (pc - 1) & 0xf);
			exit(EXIT_FAILURE);
			break;
		}

		count++;
	}
}

/*
 * Handle command line arguments.
 *
 * For now that's just "-m max_inst" to set the maximum number of instructions
 * to execute.
 *
 * Note that getopt is not strictly-speaking part of the C standard library, so
 * students are not allowed to use it in their code. But that's okay since
 * students don't have to write the simulator either.
 */
static void args(int argc, char *argv[])
{
	int opt;
	max_inst = INT_MAX;
	while ((opt = getopt(argc, argv, "m:")) != -1) {
		switch (opt) {
		case 'm':
			max_inst = atoi(optarg); /* TODO strtol */
			break;
		default:
			fprintf(stderr, "Usage: %s [-m max_inst]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
}

/*
 * Main program. Not much left to do here.
 */
int main(int argc, char *argv[])
{
	args(argc, argv);
	load();
	printf("object file (%d bytes) loaded\n", size);
	printf("intermediate SCRAM states (before each instruction)\n");
	run();
	printf("final SCRAM state (after last instruction)\n");
	dump();
	printf("\n");
	printf("processed %d instructions\n", count);
	return EXIT_SUCCESS;
}
