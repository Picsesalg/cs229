#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <ctype.h>

#include "opc.h"

#define OPCODES 3
#define MAX_MEM 16
#define MAX_CHAR 32
#define MAX_LINE 128

struct Address {
	int own_addr;
	char label[200];
	char opc[200];
	char addr[200];
};

struct Scram {
	struct Address memory[MAX_MEM + 1];
};

struct Scram load_data();
//struct Scram valid_addr(struct Scram);
void valid_addr(struct Scram);
void print(struct Scram info);
int search(struct Scram info, char *word, int line_num);
int check_digit(char *address);
char converted(struct Scram info, int index);

int main() {
	struct Scram info;
	for (int i = 0; i < MAX_MEM + 1; i++) {
		info.memory[i].own_addr = 0;
		info.memory[i].label[0] = '\0';
                info.memory[i].opc[0] = '\0';
                info.memory[i].addr[0] = '\0';
	}
	info = load_data();
	//Everything in is legal now.
	//info = valid_addr(info);
	print(info);
	exit(EXIT_SUCCESS);
}

struct Scram load_data() {
	struct Scram info;
	char *line = (char *) malloc(sizeof(char) * 201);
	char *temp = (char *) malloc(sizeof(char) * 201);
	int line_num = 0;
	char *word;
	char *delimiters = " \n\t\r\v\f";
	int label_flag = 0; //Changes to 1 when a label is in
	int opc_flag = 0; //Changes to 1 when a label is in
	int addr_flag = 0; //Changes to 1 when an address is in
	int input_flag = 0;

	for (int i = 0; i < MAX_MEM + 1; i++) {
		info.memory[i].own_addr = 0;
		info.memory[i].label[0] = '\0';
		info.memory[i].opc[0] = '\0';
		info.memory[i].addr[0] = '\0';
	}

	//Goes through each line
	while (fgets(line, 201, stdin) != NULL) {
		label_flag = 0;
		opc_flag = 0;
		addr_flag = 0;
		if (strlen(line) > MAX_LINE) {
			free(line);
			free(temp);
			fprintf(stderr, "Line too long.\n");
			exit(EXIT_FAILURE);
		}//In sas.c at line 320

		//In the line, goes through each word
		word = strtok(line, delimiters);
		
		//Empty line
		if (word == NULL) {
			continue;
		}
		while (word != NULL) {
			input_flag = 1;
			//Comment. Just ignore.
			if (word[0] == '#') {
				break;
			}
			
			//A label is too long.
			if (strlen(word) > MAX_CHAR) {
				free(line);
				free(temp);
				fprintf(stderr, "Label too long.\n");
				exit(EXIT_FAILURE);
			}
			//It's a label
			else if (word[strlen(word) - 1] == ':') {
				if (label_flag == 1) {
					free(line);
					free(temp);
					fprintf(stderr, "2 labels in 1 line.\n");
					exit(EXIT_FAILURE);
				}
				free(temp);
				temp = calloc(sizeof(temp), sizeof(char) * (strlen(word) + 1));
				strncpy(temp, word, strlen(word) - 1);
				int exists = search(info, temp, line_num);
				//It doesn't already exist in the scram
				if (exists != -1) {
					free(line);
					free(temp);
					fprintf(stderr, "Label already in.\n");
					exit(EXIT_FAILURE);
				}
				strcpy(info.memory[line_num].label, temp);
				label_flag = 1; //No more labels allowed on this line
			}

			//It's an OPCODE
			else if (opc_flag == 0 && addr_flag == 0) {
				if (!opc_valid(word)) {
					free(line);
					free(temp);
					fprintf(stderr, "Opcode not valid.\n");
					exit(EXIT_FAILURE);
				}
				strcpy(info.memory[line_num].opc, word);
				opc_flag += 1;
			}

			//It's an address. For now, just take it in.
			//Do address validating later.
			else if (opc_flag == 1 && addr_flag == 0) {
				strcpy(info.memory[line_num].addr, word);
				addr_flag += 1;
			}
			else {
				free(line);
				free(temp);
				fprintf(stderr, "Something went wrong.\n");
				exit(EXIT_FAILURE);
			}
			word = strtok(NULL, delimiters);
		}
		//Increment only if there is an OPC and address in it.
		if (opc_flag == 1 && addr_flag == 0) {
			free(line);
			free(temp);
			fprintf(stderr, "No address.\n");
			exit(EXIT_FAILURE);
		}
		if (opc_flag == 1 && addr_flag == 1) {
			info.memory[line_num].own_addr = line_num;
			line_num++;
			if (line_num > MAX_MEM) {
				free(line);
				free(temp);
				fprintf(stderr, "File exceeds 16 bytes.\n");
				exit(EXIT_FAILURE);
			}
		}

	}
	if (input_flag == 0) {
		free(line);
		free(temp);
		char *lol = "";
		printf("%s", lol);
		exit(EXIT_SUCCESS);
	}
	free(line);
	free(temp);
	valid_addr(info);
	return info;
}


//Sorts through teh addresses to see which ones are valid.
void valid_addr(struct Scram info) {
	struct Address a;
	for (int i = 0; i < MAX_MEM; i++) {
		a = info.memory[i];

		//If the opcode is a normal one
		if (opc_encode(a.opc) != -1) {
			//If the address is a value
			int digit_flag = check_digit(a.addr);
			if (digit_flag != -1) {
				if (digit_flag >= MAX_MEM) {
					fprintf(stderr, "Address is larger than 15.\n");
					exit(EXIT_FAILURE);
				}
			}

			//A label address.
			else {
				digit_flag = search(info, a.addr, -1);
				if (digit_flag == -1) {
					fprintf(stderr, "Label doesn't exist.\n");
					exit(EXIT_FAILURE);
				}
			}
		}

		//If the opcode is DAT
		else {
			//If the address is a value
			int digit_flag = check_digit(a.addr);
			if (digit_flag != -1) {
				if (digit_flag > 255) {
					fprintf(stderr, "Address longer than 255.\n");
					exit(EXIT_FAILURE);
				}
			}
			//A label address
			else {
				digit_flag = search(info, a.addr, -1);
				if (digit_flag == -1) { 
					fprintf(stderr, "Label doesn't exist.\n");
			
				}
			}
		}
	}
	//return info;
}

//Prints out the answers
void print(struct Scram info) {
	char character;

	for (int i = 0; i < MAX_MEM; i++) {
		//For the first address
		if (info.memory[i].own_addr == 0 && i == 0) {
			character = converted(info, i);
			putchar(character);
		}
		else if (info.memory[i].own_addr != 0) {
			character = converted(info, i);
			putchar(character);
		}
	}
}

//Searches for the label and returns the address.
//Else returns -1.
int search(struct Scram info, char *word, int line_num) {
	if (line_num == 0) {
		return -1;
	}
	for (int i = 0; i < MAX_MEM + 1; i++) {
		if (strcmp(info.memory[i].label, word) == 0) {
			return i;
		}
	}
	return -1;
}

//Checks if it's a number. 
//If it is, returns the number.
//Else, returns -1.
int check_digit(char *address) {
	int number;
	for (unsigned int i = 0; i < strlen(address); i++) {
		if (i == 0 && address[i] == '+') {
			continue;
		}
		else if (!isdigit(address[i])) {
			return -1;
		}
	}
	number = atoi(address);
	return number;
}

//Converts the address to char
char converted(struct Scram info, int index) {
	uint8_t first;
	uint8_t second;
	int temp;
	int actual_char;

	temp = opc_encode(info.memory[index].opc);

	first = (temp << 4) & 0xf0;

	//If the instruction isn't DAT
	if (temp != -1) {
		first = (temp << 4) & 0xf0;
		temp = check_digit(info.memory[index].addr);
		//If it's a value
		if (temp != -1) {
			second = temp & 0xf;
		}
		//If it's a label
		else {
			temp = search(info, info.memory[index].addr, -1);
			second = temp & 0xf;
		}
		actual_char = first | second;
	}
	//If the instruction is a DAT
	else {
		temp = check_digit(info.memory[index].addr);
		//If the address is a value
		if (temp != -1) {
			actual_char = temp;
		}
		//If the address is a label
		else {
			temp = search(info, info.memory[index].addr, -1);
			actual_char = temp;
		}
	}

	return actual_char;
}
