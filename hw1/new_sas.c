#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <ctype.h>

#include "opc.h"

#define OPCODES 3
#define MAX_MEM 16
#define MAX_CHAR 32
#define MAX_LINE 128

struct Address {
	int own_addr;
	char label[MAX_CHAR + 1];
	char opc[OPCODES + 1];
	char ref_addr[MAX_CHAR + 1];
};

struct Scram {
	struct Address memory[MAX_MEM];
};

struct Scram load_data();

int main() {
	struct Scram info = load_data();
	return (EXIT_SUCCESS);
}
struct Scram load_data() {
	struct Scram info;
	char *line = (char *) malloc(sizeof(char) * (MAX_LINE + 1));

	for (int i = 0; i < MAX_MEM; i++) {
		while(fgets(line, 129, stdin) != NULL) {
			printf("%s %d", line, strlen(line));
			if (strlen(line) >= MAX_LINE) {
				fprintf(stderr, "Line length exceeded.\n");
				exit(EXIT_FAILURE);
			}
		}
	}

}
