/**
 * Alice Yang
 * ayang36@jhu.edu
 * associate.h
 */

#ifndef ASSOCIATE_H
#define ASSOCIATE_H

#include <map>
#include <string>
#include <vector>

#include "block.h"
#include "cache.h"

class associate : public cache
{

public:
	associate(int s, int bl, int by, std::string a, std::string b, std::string c)
	: cache(s, bl, by, a, b)
	{
		if (c.compare("lru") == 0) {
			evict = 'l';
		} else if (c.compare("fifo") == 0) {
			evict = 'f';
		} else {
			evict = 'r';
		}
	}

	void load(int add);
	void load_hit(int index, int offset);
	void load_miss(int index, int tag);
	void store(int add);
	void store_hit(int index, int offset);
	void store_miss(int index, int tag);
	void print();

private:
	char evict;
	std::map<int, std::vector<block>> cash;
};
#endif
