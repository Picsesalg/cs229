/**
 * Alice Yang
 * ayang36@jhu.edu
 * direct.cpp
 */

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "block.h"
#include "cache.h"
#include "direct.h"

using std::cout;
using std::log2;
using std::map;
using std::pair;
using std::string;

void direct::load(int add)
{
	lod_tot++;

	int temp = log2(bytes);
	add = add >> temp;
	int index = add & (sets - 1);
	temp = log2(sets);
	int tag = add >> temp;

	map<int, block>::iterator it = cash.find(index);
	
	if (it == cash.end()) {
		load_miss(index, tag);
	} else {
		if (it->second.get_tag() == tag) {
			load_hit();
		} else {
			load_miss(index, tag);
		}
	}
}

void direct::load_hit()
{
	lod_hit++;
	cyc_tot++;
}

void direct::load_miss(int index, int tag)
{
	lod_mis++;

	block b = block(tag);

	map<int, block>::iterator it = cash.find(index);

	if (it == cash.end()) {
		cash.insert(pair<int, block>(index, b));
		cyc_tot += bytes / 4 * 100;
	} else {
		if (it->second.get_dirty() == 1) {
			cyc_tot += bytes / 4 * 100;
		}
		it->second = b;
		cyc_tot += bytes / 4 * 100;
	}
	cyc_tot++;
}

void direct::store(int add)
{
	str_tot++;

	int temp = log2(bytes);
    add = add >> temp;
    int index = add & (sets - 1);
    temp = log2(sets);
    int tag = add >> temp;

	map<int, block>::iterator it = cash.find(index);

	if (it == cash.end()) {
		store_miss(index, tag);
	} else {
		if (it->second.get_tag() == tag) {
			store_hit(index);
		} else {
			store_miss(index, tag);
		}
	}
}

void direct::store_hit(int index)
{
	str_hit++;

	map<int, block>::iterator it = cash.find(index);

	if (write == 't') {
		cyc_tot++;
		cyc_tot += bytes / 4 * 100;
	} else {
		it->second.set_dirty();
		cyc_tot++;
	}
}

void direct::store_miss(int index, int tag)
{
	str_mis++;

	if (alloc == 'n') {
		cyc_tot += bytes / 4 * 100;
		return;
	}

	map<int, block>::iterator it = cash.find(index);

	block b = block(tag);

	/* Inserting new block in. */
	if (it == cash.end()) {
		cash.insert(pair<int, block>(index, b));
		cyc_tot += bytes / 4 * 100;
	} else {
		if (it->second.get_dirty() == 1) {
			cyc_tot += bytes / 4 * 100;
		}
		it->second = b;
		cyc_tot += bytes / 4 * 100;
	}

	it = cash.find(index);

	if (write == 't') {
		cyc_tot += bytes / 4 * 100;
		cyc_tot++;
	} else {
		it->second.set_dirty();
		cyc_tot++;
	}
}

void direct::print()
{
	cout << "Total loads: " << lod_tot << "\n";
    cout << "Total stores: " << str_tot << "\n";
    cout << "Load hits: " << lod_hit << "\n";
    cout << "Load misses: " << lod_mis << "\n";
    cout << "Store hits: " << str_hit << "\n";
    cout << "Store misses: " << str_mis << "\n";
    cout << "Total cycles: " << cyc_tot << "\n";
}
