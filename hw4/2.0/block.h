/**
 * Alice Yang
 * ayang36@jhu.edu
 * block.h
 */

#ifndef BLOCK_H
#define BLOCK_H

#include <iostream> //TODO remove after

class block {

public:

	block(int t)
	{
		dirty = 0;
		tag = t;
	}

	int get_dirty(); 
	void set_dirty();
	int get_tag();

private:
	int dirty;
	int tag;
};

#endif
