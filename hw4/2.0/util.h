/**
 * Alice Yang
 * ayang36@jhu.edu
 * util.h
 */

#ifndef UTIL_H
#define UTIL_H

#include <string>

#include "associate.h"
#include "cache.h"
#include "direct.h"

int check_arguments(int argc, char *argv[]);
int val_power(int arg, int min);
cache direct_read_file(int s, int bl, int by, std::string a, std::string b);
cache associate_read_file(int s, int bl, int by, std::string a, std::string b, std::string c);
void val_ls(std::string a);
int val_add(std::string a);

#endif
