/**
 * Alice Yang
 * ayang36@jhu.edu
 * direct.h
 */

#ifndef DIRECT_H
#define DIRECT_H

#include <map>

#include "block.h"
#include "cache.h"

class direct : public cache
{

public:
	direct(int s, int bl, int by, std::string a, std::string b)
	: cache(s, bl, by, a, b) {}

	void load(int add);
	void load_hit();
	void load_miss(int index, int tag);
	void store(int add);
	void store_hit(int index);
	void store_miss(int index, int tag);
	void print();

private:
	std::map<int, block> cash;
};

#endif
