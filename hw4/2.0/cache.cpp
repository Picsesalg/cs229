/**
 * Alice Yang
 * ayang36@jhu.edu
 * cache.cpp
 */

#include <string>

#include "cache.h"

using std::string;

string cache::toString() {
	string s = "Total loads: " + lod_tot;
	s += "\nTotal stores: " + str_tot;
	s += "\nLoad hits: " + lod_hit;
	s += "\nLoad misses: " + lod_mis;
	s += "\nStore hits: " + str_hit;
	s += "\nStore misses: " + str_mis;
	s += "\nTotal cycles: " + cyc_tot;
	return s;
}
