/**
 * Alice Yang
 * ayang36@jhu.edu
 * csim.cpp
 */

#include <iostream>
#include <string>

#include "util.h"

using std::stoi;
using std::string;

int main(int argc, char *argv[])
{

	check_arguments(argc, argv);

	int sets = stoi(argv[1]);
    int blocks = stoi(argv[2]);
    int bytes = stoi(argv[3]);
    string allocate = string(argv[4]);
    string write = string(argv[5]);
    string eviction = "";

	if (blocks == 1) {
		direct_read_file(sets, blocks, bytes, allocate, write);
	} else {
		eviction = string(argv[6]);
		associate_read_file(sets, blocks, bytes, allocate, write, eviction);
	}

	return 0;
}
