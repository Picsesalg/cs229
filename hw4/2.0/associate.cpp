/**
 * Alice Yang
 * ayang36@jhu.edu
 * associate.cpp
 */

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "associate.h"
#include "block.h"
#include "cache.h"

using std::cout;
using std::log2;
using std::map;
using std::pair;
using std::vector;

void associate::load(int add)
{
	lod_tot++;

	int temp = log2(bytes);
	add = add >> temp;
	int index = add & (sets - 1);
	temp = log2(sets);
	int tag = add >> temp;

	map<int, vector<block>>::iterator it = cash.find(index);

	if (it == cash.end()) {
		load_miss(index, tag);
	} else {
		for (unsigned int i = 0; i < it->second.size(); i++) {
			if (it->second.at(i).get_tag() == tag) {
				load_hit(index, i);
				return;
			}
		}
		load_miss(index, tag);
	}
}

void associate::load_hit(int index, int offset)
{
	lod_hit++;
	cyc_tot++;

	if (evict == 'l') {
		map<int, vector<block>>::iterator it = cash.find(index);
		block b = it->second.at(offset);
		it->second.erase(it->second.begin() + offset);
		it->second.push_back(b);
	}
}

void associate::load_miss(int index, int tag)
{
	lod_mis++;

	block b = block(tag);

	map<int, vector<block>>::iterator it = cash.find(index);

	/* Not full. The set has nothing in it. */
	if (it == cash.end()) {
		cyc_tot++;
		cyc_tot += bytes / 4 * 100;
		vector<block> v = vector<block>();
		v.push_back(b);
		cash.insert(pair<int, vector<block>>(index, v));
		return;
	}

	/* Not full but with things in. */
	if (it->second.size() < (unsigned int) blocks) {
		cyc_tot++;
		cyc_tot += bytes / 4 * 100;
		it->second.push_back(b);
		return;
	} else { /* Full. Need to evict. */
		int i = 0;
		if (evict == 'r') {
			i = rand() % blocks;
		}
		block temp = it->second.at(i);
		if (temp.get_dirty() == 1) {
			cyc_tot += bytes / 4 * 100;
		}
		it->second.erase(it->second.begin() + i);
		it->second.push_back(b);
		cyc_tot += bytes / 4 * 100;
		cyc_tot++;
		return;
	}
}

void associate::store(int add)
{
	str_tot++;

	int temp = log2(bytes);
	add = add >> temp;
	int index = add & (sets - 1);
	temp = log2(sets);
	int tag = add >> temp;

	map<int, vector<block>>::iterator it = cash.find(index);

	if (it == cash.end()) {
		store_miss(index, tag);
	} else {
		for (unsigned int i = 0; i < it->second.size(); i++) {
			if (it->second.at(i).get_tag() == tag) {
				store_hit(index, i);
				return;
			}
		}
		store_miss(index, tag);
	}
}

void associate::store_hit(int index, int offset)
{
	str_hit++;

	map<int, vector<block>>::iterator it = cash.find(index);

	if (write == 't') {
		cyc_tot++;
		cyc_tot += bytes / 4 * 100;
	} else {
		cyc_tot++;
		it->second.at(offset).set_dirty();
	}

	if (evict == 'l') {
		block b = it->second.at(offset);
		it->second.erase(it->second.begin() + offset);
		it->second.push_back(b);
	}
}

void associate::store_miss(int index, int tag)
{
	str_mis++;

	if (alloc == 'n') {
		cyc_tot += bytes / 4 * 100;
		return;
	}

	map<int, vector<block>>::iterator it = cash.find(index);

	block b = block(tag);

	if (it == cash.end()) {
		vector<block> v = vector<block>();
		v.push_back(b);
		cash.insert(pair<int, vector<block>>(index, v));
		cyc_tot += bytes / 4 * 100;
	} else if (it->second.size() < (unsigned int) blocks) {
		it->second.push_back(b);
		cyc_tot += bytes / 4 * 100;
	} else {
		int i = 0;
		if (alloc == 'r') {
			i = rand() % blocks;
		}
		if (it->second.at(i).get_dirty() == 1) {
			cyc_tot += bytes / 4 * 100;
		}
		it->second.erase(it->second.begin() + i);
		it->second.push_back(b);
		cyc_tot += bytes / 4 * 100;
	}

	it = cash.find(index);

	if (write == 't') {
		cyc_tot++;
		cyc_tot += bytes / 4 * 100;
	} else {
		cyc_tot++;
		it->second.back().set_dirty();
	}
}

void associate::print()
{
        cout << "Total loads: " << lod_tot << "\n";
        cout << "Total stores: " << str_tot << "\n";
        cout << "Load hits: " << lod_hit << "\n";
        cout << "Load misses: " << lod_mis << "\n";
        cout << "Store hits: " << str_hit << "\n";
        cout << "Store misses: " << str_mis << "\n";
        cout << "Total cycles: " << cyc_tot << "\n";
}
