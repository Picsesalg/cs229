/**
 * Alice Yang
 * ayang36@jhu.edu
 * util.cpp
 */

#include <cctype>
#include <iostream>
#include <sstream>
#include <string>

#include "associate.h"
#include "cache.h"
#include "direct.h"
#include "util.h"

using std::cerr;
using std::cin;
using std::cout;
using std::getline;
using std::hex;
using std::istringstream;
using std::stoi;
using std::string;
using std::stringstream;

int check_arguments(int argc, char *argv[]) 
{
	if (argc != 6) {
		if (argc != 7) {
                	cerr << "Wrong number of arguments.\n";
                	exit(EXIT_FAILURE);
		}
    }
    if (!val_power(stoi(argv[1]), 0)) {
    	cerr << "Set number invalid.\n";
            exit(EXIT_FAILURE);
    }
    if (!val_power(stoi(argv[2]), 0)) {
        cerr << "Block number invalid.\n";
        exit(EXIT_FAILURE);
    }
    if (!val_power(stoi(argv[3]), 3)) {
        cerr << "Byte number invalid.\n";
        exit(EXIT_FAILURE);
    }
    if (string(argv[4]).compare("write-allocate") != 0) {
		if (string(argv[4]).compare("no-write-allocate") != 0) {
			cerr << "Write allocate incorrect.\n";
            exit(EXIT_FAILURE);
		}
    }
    if (string(argv[5]).compare("write-through") != 0) {
    	if (string(argv[5]).compare("write-back") != 0) {
        	cerr << "Write through back is wrong.\n";
            exit(EXIT_FAILURE);
		}
	}
    if (argc == 7) {
     	if (string(argv[6]).compare("lru") != 0) {
			if (string(argv[6]).compare("fifo") != 0) {
                if (string(argv[6]).compare("random") != 0) {
                	cerr << "Last arg invalid.\n";
                    exit(EXIT_FAILURE);
                }
			}
		}
    }
    if (string(argv[4]).compare("no-write-allocate") == 0
        && string(argv[5]).compare("write-back") == 0) {
        	cerr << "Combination doesn't make sense.\n";
            exit(EXIT_FAILURE);
	}
	return 1;
}

int val_power(int arg, int min)
{
    if (arg <= min) {
                return 0;
        }

        if ((arg & (arg - 1)) == 0) {
                return 1;
        } else {
                return 0;
        }
}

cache direct_read_file(int s, int bl, int by, string a, string b)
{

	direct cash = direct(s, bl, by, a, b);

	string line = "";
	string ls = "";
	string add = "";

	while (getline(cin, line)) {
		int flag = 1;
		for (unsigned int i = 0; i < line.length(); i++) {
			if (!isspace(line[i])) {
				flag = 0;
				break;
			}
		}
		if (flag) {
			continue;
		}
		istringstream iss(line);
		if (iss.eof()) {
			cerr << "Line formatted wrong.\n";
			exit(EXIT_FAILURE);
		}
		iss >> ls;
		val_ls(ls);
		if (iss.eof()) {
			cerr << "Line formate incorrect.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add;
		int address = val_add(add);
		if (ls.compare("l") == 0) {
			cash.load(address);
		} else {
			cash.store(address);
		}
	}

	cash.print();

	return cash;
}

cache associate_read_file(int s, int bl, int by, string a, string b, string c)
{

	associate cash = associate(s, bl, by, a, b, c);

	string line = "";
	string ls = "";
	string add = "";

	while (getline(cin, line)) {
		int flag = 1;
		for (unsigned int i = 0; i < line.length(); i++) {
			if (!isspace(line[i])) {
				flag = 0;
				break;
			}
		}
		if (flag) {
			continue;
		}
		istringstream iss(line);
		if (iss.eof()) {
			cerr << "Line formatted wrong.\n";
			exit(EXIT_FAILURE);
		}
		iss >> ls;
		val_ls(ls);
		if (iss.eof()) {
			cerr << "Line formate incorrect.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add;
		int address = val_add(add);
		if (ls.compare("l") == 0) {
			cash.load(address);
		} else {
			cash.store(address);
		}
	}

	cash.print();
	return cash;
}
void val_ls(string a)
{
	if (a.compare("l") == 0) {
		return;
	} else if (a.compare("s") == 0) {
		return;
	} else {
		cerr << "Incorrect format.\n";
		exit(EXIT_FAILURE);
	}
}

int val_add(string a)
{
	if (a.substr(0, 2).compare("0x") != 0) {
		cerr << "Address wrong.\n";
		exit(EXIT_FAILURE);
	}
	a = a.substr(2);
	for (unsigned int i = 0; i < a.length(); i++) {
		if (!isxdigit(a[i])) {
			cerr << "Address wrong.\n";
			exit(EXIT_FAILURE);
		}
	}
	int address = 0;
	stringstream ss(a);
	ss << hex << a;
	ss >> address;
	if (address < 0) {
		cerr << "Address wrong.\n";
		exit(EXIT_FAILURE);
	}
	return address;
}
