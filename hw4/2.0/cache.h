/**
 * ALice Yang
 * ayang36@jhu.edu
 * cache.h
 */

#ifndef CACHE_H
#define CACHE_H

#include <string>

class cache {

public:

	cache() {}

	cache(int s, int bl, int by, std::string a, std::string b) 
	{
		sets = s;
		blocks = bl;
		bytes = by;
		if (a.compare("write-allocate") == 0) {
			alloc = 'a';
		} else {
			alloc = 'n';
		}
		if (b.compare("write-through") == 0) {
			write = 't';
		} else {
			write = 'b';
		}
		lod_tot = 0;
		str_tot = 0;
		lod_hit = 0;
		lod_mis = 0;
		str_hit = 0;
		str_mis = 0;
		cyc_tot = 0;
	}

	void load(int add);
	void store(int add);
	std::string toString();

protected:
	int sets;
	int blocks;
	int bytes;
	char alloc;
	char write;
	int lod_tot;
	int str_tot;
	int lod_hit;
	int lod_mis;
	int str_hit;
	int str_mis;
	int cyc_tot;
};

#endif
