/**
 * ALice Yang
 * ayang36@jhu.edu
 * block.h
 */

class block
{
    
public:
    block(unsigned long t, unsigned long counter)
    {
        tag = t;
        dirty = 0;
        time = counter;
    }

    unsigned long get_tag();
    int get_dirty();
    void set_dirty();
    unsigned long get_time();
    void set_time(unsigned long t);

private:
    unsigned long tag;
    int dirty;
    unsigned long time;
};