/**
 * Alice Yang
 * ayang36@jhu.edu
 * cache.cpp
 */

#include <cmath>
#include <iostream>
#include <map>
#include <utility>
#include <vector>

//#include "block.h"
#include "cache.h"

using std::cerr;
using std::cout;
using std::log2;
using std::map;
using std::pair;
using std::vector;

void cache::load(unsigned long add)
{
    COUNTER++;

    lod_tot++;

    int temp = log2(bytes);
	add = add >> temp;
	int index = add & (sets - 1);
	temp = log2(sets);
	unsigned long tag = add >> temp;

    map<unsigned long, vector<block>>::iterator it = cash.find(index);

    if (it == cash.end()) {
        load_miss(index, tag);
        return;
    }
    for (unsigned int i = 0; i < it->second.size(); i++) {
        if (it->second.at(i).get_tag() == tag) {
            load_hit(index, i);
            return;
        }
    }
    load_miss(index, tag);
}

void cache::load_hit(int index, int offset)
{
    lod_hit++;

    cyc_tot++;

    if (evict == 'l') {
        map<unsigned long, vector<block>>::iterator it = cash.find(index);
        //block b = it->second.at(offset);
        it->second.at(offset).set_time(COUNTER);
        /*it->second.erase(it->second.begin() + offset);
        it->second.push_back(b)*/;
    }
}

void cache::load_miss(int index, unsigned long tag)
{
    lod_mis++;

    block b = block(tag, COUNTER);
    map<unsigned long, vector<block>>::iterator it = cash.find(index);

    if (it == cash.end()) {
        vector<block> v = vector<block>();
        v.push_back(b);
        cash.insert(pair<int, vector<block>>(index, v));
        cyc_tot += bytes * 100 / 4;
    } else if (it->second.size() < bytes) {
        it->second.push_back(b);
        cyc_tot += bytes * 100 / 4;
    } else if (it->second.size() == bytes) {
        int i = 0;
        if (evict == 'r') {
            i = rand() % blocks;
        } else {
            unsigned long min = it->second.at(0).get_time();
            for (unsigned int j = 1; j < it->second.size(); j++) {
                if (it->second.at(j).get_time() < min) {
                    min = it->second.at(j).get_time();
                    i = j;
                }
            }
        }
        if (write == 'b' && it->second.at(i).get_dirty() == 1) {
            cyc_tot += bytes * 100 / 4;
        }
        it->second.erase(it->second.begin() + i);
        it->second.push_back(b);
        cyc_tot += bytes * 100 / 4;
    } /*else {
        cerr << "Something is wrong\n";
        exit(EXIT_FAILURE);
    }*/
    cyc_tot++;
}

void cache::store(unsigned long add)
{
    COUNTER++;
    str_tot++;

    int temp = log2(bytes);
	add = add >> temp;
	int index = add & (sets - 1);
	temp = log2(sets);
	int tag = add >> temp;

    map<unsigned long, vector<block>>::iterator it = cash.find(index);

    if (it == cash.end()) {
        store_miss(index, tag);
        return;
    }
    for (unsigned int i = 0; i < it->second.size(); i++) {
        if (it->second.at(i).get_tag() == tag) {
            store_hit(index, i);
            return;
        }
    }
    store_miss(index, tag);
}

void cache::store_hit(int index, int offset)
{
    str_hit++;

    map<unsigned long, vector<block>>::iterator it = cash.find(index);

    if (write == 'b') {
        it->second.at(offset).set_dirty();
        cyc_tot++;
    } else if (write == 't') {
        cyc_tot++;
        cyc_tot += bytes * 100 / 4;
    } /*else {
        cerr << "SOmething's wrong.\n";
        exit(EXIT_FAILURE);
    }*/

    if (evict == 'l') {
        map<unsigned long, vector<block>>::iterator it = cash.find(index);
        //block b = it->second.at(offset);
        //it->second.erase(it->second.begin() + offset);
        //it->second.push_back(b);
        it->second.at(offset).set_time(COUNTER);
    }
}

void cache::store_miss(int index, int tag)
{
    str_mis++;

    if (alloc == 'n') {
        cyc_tot += bytes * 100 / 4;
        return;
    }

    block b = block(tag, COUNTER);

    map<unsigned long, vector<block>>::iterator it = cash.find(index);

    /* Brining new block in and evicting if necessary. */
    if (it == cash.end()) {
        vector<block> v = vector<block>();
        v.push_back(b);
        cash.insert(pair<int, vector<block>>(index, v));
        cyc_tot += bytes * 100 / 4;
    } else if (it->second.size() < bytes) {
        it->second.push_back(b);
        cyc_tot += bytes * 100 / 4;
    } else if (it->second.size() == bytes) {
        int i = 0;
        if (evict == 'r') {
            i = rand() % blocks;
        }
        if (write == 'b' && it->second.at(i).get_dirty() == 1) {
            cyc_tot += bytes * 100 / 4;
        }
        it->second.erase(it->second.begin() + i);
        it->second.push_back(b);
        cyc_tot += bytes * 100 / 4;
    } /*else {
        cerr << "Something happened :( \n";
        exit(EXIT_FAILURE);
    }*/

    it = cash.find(index);

    if (write == 'b') {
        it->second.back().set_dirty();
        cyc_tot++;
    } else {
        cyc_tot++;
        cyc_tot += bytes * 100 / 4;
    }
}

void cache::print()
{
    cout << "Total load: " << lod_tot << "\n";
    cout << "Total store: " << str_tot << "\n";
    cout << "Load hits: " << lod_hit << "\n";
    cout << "Load misses: " << lod_mis << "\n";
    cout << "Store hits: " << str_hit << "\n";
    cout << "Store misses: " << str_mis << "\n";
    cout << "Total cycles " << cyc_tot << "\n";
}