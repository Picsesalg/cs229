/**
 * ALice Yang
 * ayang36@jhu.edu
 * block.cpp
 */

#include "block.h"

unsigned long block::get_tag()
{
    return tag;
}

int block::get_dirty()
{
    return dirty;
}

void block::set_dirty()
{
    dirty = 1;
}

unsigned long block::get_time()
{
    return time;
}

void block::set_time(unsigned long t) 
{
    time = t;
}