/**
 * Alice Yang
 * ayang36@jhu.edu
 * util.h
 */

void check_arguments(int argc, char *argv[]);
int val_power(int arg, int min);
void read_data(int a, int b, int c, std::string d, std::string e, std::string f);
void val_ls(std::string s);
int val_add(std::string s);