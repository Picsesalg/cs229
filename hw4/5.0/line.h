/**
 * Alice Yang
 * ayang36@jhu.edu
 * line.h
 */

#ifndef LINE_H
#define LINE_H

#include <vector>

#include "block.h"

class line
{

public:

    line();

    int get_valid();
    void set_valid();
    std::vector<block> get_row();
    void set_row(std::vector<block> v);

private:
    int valid;
    std::vector<block> blocks;
};

#endif