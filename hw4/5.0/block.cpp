/**
 * Alice Yang
 * ayang36@jhu.edu
 * block.cpp
 */

#include "block.h"

block::block()
{
    tag = -1;
    dirty = 0;
}

block::block(unsigned long t)
{
    tag = t;
    dirty = 0;
}

unsigned long block::get_tag()
{
    return tag;
}

void block::set_tag(unsigned long t)
{
    tag = t;
}

int block::get_dirty()
{
    return dirty;
}

void block::set_dirty()
{
    dirty = 1;
}