/**
 * Alice Yang
 * ayang36@jhu.edu
 * util.h
 */

#ifndef UTIL_H
#define UTIL_H 

#include "cache.h"

void check_arguments(int argc, char *argv[]);
int val_power(int arg, int min);
void read_file(cache cash);
void val_ls(std::string ls);
unsigned long val_add(std::string s);

#endif