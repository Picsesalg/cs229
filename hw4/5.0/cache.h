/**
 * Alice Yang
 * ayang36@jhu.edu
 * cache.h
 */

#ifndef CACHE_H
#define CACHE_H

#include<string>
#include <vector>

#include "line.h"

class cache
{

public:

    cache(unsigned long s, unsigned long bl, unsigned long by, std::string wa, std::string wbwt, std::string e);

    void hit_miss(std::string ls, unsigned long address);
    void load_hit(unsigned long index, unsigned long tag);
    void load_miss();
    void store_hit();
    void store_miss();
    void print();
    
private:
    unsigned long set_num;
    unsigned long block_num;
    unsigned long byte_num;
    char alloc;
    char write;
    char evict;
    unsigned long lod_tot;
    unsigned long str_tot;
    unsigned long lod_hit;
    unsigned long lod_mis;
    unsigned long str_hit;
    unsigned long str_mis;
    unsigned long cyc_tot;
    std::vector<line> lines;
};

#endif