/**
 * Alice Yang
 * ayang36@jhu.edu
 * line.cpp
 */

#include "line.h"

using std::vector;

line::line()
{
    valid = 0;
    blocks = vector<block>();
}

int line::get_valid()
{
    return valid;
}

void line::set_valid()
{
    valid = 1;
}

vector<block> line::get_row()
{
    return blocks;
}

void line::set_row(vector<block> v)
{
    blocks = v;
}