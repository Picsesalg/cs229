/**
 * Alice Yang
 * ayang36@jhu.edu
 */

#include <cmath>
#include <iostream>

#include "cache.h"

using std::cout;
using std::log2;
using std::string;
using std::vector;

cache::cache(unsigned long s, unsigned long bl, unsigned long by, string wa, string wbwt, string e)
{
    set_num = s;
    block_num = bl;
    byte_num = by;
    if (wa.compare("write-allocate") == 0) {
            alloc = 'w';
    } else {
            alloc = 'n';
    }
    if (wbwt.compare("write-back") == 0) {
        write = 'b';
    } else {
        write = 't';
    }
    if (e.compare("lru") == 0) {
        evict = 'l';
    } else if (e.compare("fifo") == 0) {
        evict = 'f';
    } else {
        evict = 'r';
    }
    lod_tot = 0;
    str_tot = 0;
    lod_hit = 0;
    lod_mis = 0;
    str_hit = 0;
    str_mis = 0;
    cyc_tot = 0;
    lines = vector<line>(block_num, line());
}

void cache::hit_miss(string ls, unsigned long address)
{
    int temp = log2(byte_num);
	address = address >> temp;
	unsigned long index = address & (set_num - 1);
	temp = log2(set_num);
	unsigned long tag = address >> temp;

    int flag = 0;

    int valid = lines.at(index).get_valid();

    if (valid == 0) { /* Automatic miss. */
        if (ls.compare("l") == 0) {
            flag = 2;
        } else {
            flag = 4;
        }
    } else { /* Index valid. Check if tag valid. */
        int got = 0;
        vector<block> v = lines.at(index).get_row();
        for (unsigned int i = 0; i < v.size(); i++) {
            if (v.at(i).get_tag() == tag) {
                got = 1;
                break;
            }
        }
        if (got == 0) { /* Tag is not valid. Miss. */
            if (ls.compare("l") == 0) {
                flag = 2;
            } else {
                flag = 4;
            }
        } else { /* Tag is valid, hit. */
            if (ls.compare("l") == 0) {
                flag = 1;
            } else {
                flag = 3;
            }
        }
    }

    switch(flag) {
        case 1:
            load_hit(index, tag);
            break;
        case 2:
            load_miss();
            break;
        case 3:
            store_hit();
            break;
        case 4:
            store_miss();
    }
}

void cache::load_hit(unsigned long index, unsigned long tag)
{
    lod_tot++;
    lod_hit++;

    int j = 0;
    block b = block();

    if (evict == 'l') {
        vector<block> v = lines.at(index).get_row();
        for (unsigned long i = 0; i < v.size(); i++) {
            if (v.at(i).get_tag() == tag) {
                b = v.at(i);
                j = i;
            }
        }
        v.erase(v.begin() + j);
        v.push_back(b);
        lines.at(index).set_row(v);
    }
}

void cache::load_miss()
{
    lod_tot++;
    lod_mis++;
}

void cache::store_hit()
{
    str_tot++;
    str_hit++;
}

void cache::store_miss()
{
    str_tot++;
    str_mis++;
}

void cache::print()
{
    cout << "Total loads: " << lod_hit << "\n";
    cout << "Total stores: " << str_tot << "\n";
    cout << "Load hits: " << lod_hit << "\n";
    cout << "Load misses: " << lod_mis << "\n";
    cout << "Store hits: " << str_hit << "\n";
    cout << "Store misses: " << str_mis << "\n";
    cout << "Total cycles: " << cyc_tot << "\n";
}