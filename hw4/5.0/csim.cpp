/**
 * Alice Yang
 * ayang36@jhu.edu
 * csim.cpp
 */

#include <string>

#include "cache.h"
#include "util.h"

using std::stoi;
using std::string;

int main(int argc, char *argv[])
{
    check_arguments(argc, argv);

    unsigned long s = stoi(argv[1]);
    unsigned long bl = stoi(argv[2]);
    unsigned long by = stoi(argv[3]);
    string al = string(argv[4]);
    string wr = string(argv[5]);
    string ev = string(argv[6]);

    cache cash = cache(s, bl, by, al, wr, ev);

    read_file(cash);

    return 0;
}