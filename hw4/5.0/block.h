/**
 * Alice Yang
 * ayang36@jhu.edu
 * block.h
 */

#ifndef BLOCK_H
#define BLOCK_H

class block
{

public:

    block();

    block(unsigned long t);

    unsigned long get_tag();
    void set_tag(unsigned long t);
    int get_dirty();
    void set_dirty();

private:

    unsigned long tag;
    int dirty;
};

#endif