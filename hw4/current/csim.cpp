/**
 * Alice Yang
 * ayang36@jhu.edu
 * csim.cpp
 */

#include <string>

#include "util.h"

using std::stoi;
using std::string;

int main(int argc, char *argv[])
{

	check_arguments(argc, argv);

	unsigned long sets = stoi(argv[1]);
    	unsigned long blocks = stoi(argv[2]);
	unsigned long bytes = stoi(argv[3]);
    	string allocate = string(argv[4]);
    	string write = string(argv[5]);
	string eviction = string(argv[6]);

	read_file(sets, blocks, bytes, allocate, write, eviction);

	return 0;
}
