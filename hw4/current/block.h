/**
 * Alice Yang
 * ayang36@jhu.edu
 * block.h
 */

#ifndef BLOCK_H
#define BLOCK_H

#include <iostream>

class block {

public:

	block();
	block(unsigned long t, unsigned long timer);

	int get_dirty(); 
	void set_dirty();
	long get_tag();
	void set_time(unsigned long timer);
	unsigned long get_time();

private:
	int dirty;
	long tag;
	unsigned long time;
};

#endif
