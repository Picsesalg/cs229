/**
 * Alice Yang
 * ayang36@jhu.edu
 * cache.cpp
 */

#include <cmath>
#include <iostream>
#include <string>

#include "cache.h"

using std::cout;
using std::log2;
using std::string;
using std::vector;

cache::cache(unsigned long s, unsigned long bl, unsigned long by, string a, string b, string c)
{
	sets = s;
        blocks = bl;
	bytes = by;
	if (a.compare("write-allocate") == 0) {
		alloc = 'a';
	} else {
		alloc = 'n';
	}
	if (b.compare("write-through") == 0) {
		write = 't';
	} else {
		write = 'b';
	}
	if (c.compare("lru") == 0) {
		evict = 'l';
	} else if (c.compare("fifo") == 0) {
		evict = 'f';
	} else {
		evict = 'r';
	}
	lod_tot = 0;
	str_tot = 0;
	lod_hit = 0;
	lod_mis = 0;
	str_hit = 0;
	str_mis = 0;
	cyc_tot = 0;
	COUNTER = 0;
	lines = vector<vector<block>>(sets, vector<block>());
}

void cache::load(unsigned long add)
{
	lod_tot++;
	COUNTER++;

	int temp = log2(bytes);
	add = add >> temp;
	unsigned long index = add & (sets - 1);
	temp = log2(sets);
	unsigned long tag = add >> temp;

	vector<block> v = lines.at(index);

	/* Valid bit not set. */
	if (v.size() == 0) {
		load_miss(index, tag);
		return;
	}

	/* Search for a match within the set. */
	for (unsigned long i = 0; i < v.size(); i++) {
		if (v.at(i).get_tag() == (long) tag) {
			load_hit(index, i);
			return;
		}
	}

	load_miss(index, tag);
	return;
}

void cache::load_hit(unsigned long index, unsigned long offset)
{
	lod_hit++;
	cyc_tot++;

	if (evict == 'l') {
		vector<block> &v = lines.at(index);
		v.at(offset).set_time(COUNTER);
	}
}

void cache::load_miss(unsigned long index, unsigned long tag)
{
	lod_mis++;
	eviction(index, tag);
	cyc_tot++;
}

void cache::store(unsigned long add)
{
	str_tot++;
	COUNTER++;

	int temp = log2(bytes);
	add = add >> temp;
	unsigned long index = add & (sets - 1);
	temp = log2(sets);
	unsigned long tag = add >> temp;

	vector<block> v = lines.at(index);

	/* Valid bit not set. */
	if (v.size() == 0) {
		store_miss(index, tag);
		return;
	}

	for (unsigned long i = 0; i < v.size(); i++) {
		if (v.at(i).get_tag() == (long) tag) {
			store_hit(index, i);
			return;
		}
	}

	store_miss(index, tag);
	return;
}

void cache::store_hit(unsigned long index, unsigned long offset)
{
	str_hit++;
	vector<block> &v = lines.at(index);

	if (write == 't') {
		cyc_tot += bytes * 100 / 4;
		cyc_tot++;
	} else {
		v.at(offset).set_dirty();
		cyc_tot++;
	}

	if (evict == 'l') {
		v.at(offset).set_time(COUNTER);
	}
}

void cache::store_miss(unsigned long index, unsigned long tag)
{
	str_mis++;

	if (alloc == 'n') {
		cyc_tot += bytes * 100 / 4;
		return;
	}

	eviction(index, tag);

	vector<block> &v = lines.at(index);
	unsigned int offset = -1;

	for (unsigned long i = 0; i < v.size(); i++) {
		if (v.at(i).get_tag() == (long) tag) {
			offset = i;
			break;
		}
	}

	if (write == 't') {
		cyc_tot += bytes * 100 / 4;
		cyc_tot++;
	} else {
		v.at(offset).set_dirty();
		cyc_tot++;
	}

	if (evict == 'l') {
		v.at(offset).set_time(COUNTER);
	}
}

void cache::eviction(unsigned long index, unsigned long tag)
{
	block b = block(tag, COUNTER);

	vector<block> &v = lines.at(index);

	/* Bringing the new block in. */
	if (v.size() < blocks) {
		v.push_back(b);
		cyc_tot += bytes * 100 / 4;
	} else if (write == 'r') {
		int i = rand() % blocks;
		if (v.at(i).get_dirty() == 1) {
			cyc_tot += bytes * 100 / 4;
		}
		v.push_back(b);
		cyc_tot += bytes * 100 / 4;
	} else {
		unsigned long i = 0;
		unsigned long j = v.at(0).get_time();
		for (unsigned int k = 0; k < v.size(); k++) {
			if (v.at(k).get_time() < j) {
				i = k;
				j = v.at(k).get_time();
			}
		}
		if (v.at(i).get_dirty() == 1) {
			cyc_tot += bytes * 100 / 4;
		}
		v.erase(v.begin() + i);
		v.push_back(b);
		cyc_tot += bytes * 100 / 4;
	}
}

void cache::print() 
{
	cout << "Total loads: "  << lod_tot;
	cout << "\nTotal stores: " << str_tot;
	cout << "\nLoad hits: " << lod_hit;
	cout << "\nLoad misses: " << lod_mis;
	cout << "\nStore hits: " << str_hit;
	cout << "\nStore misses: " << str_mis;
	cout << "\nTotal cycles: " << cyc_tot;
}
