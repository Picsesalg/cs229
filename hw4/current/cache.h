/**
 * ALice Yang
 * ayang36@jhu.edu
 * cache.h
 */

#ifndef CACHE_H
#define CACHE_H

#include <string>
#include <vector>

#include "block.h"

class cache {

public:

	cache(unsigned long s, unsigned long bl, unsigned long by, std::string a, std::string b, std::string c); 

	void load(unsigned long add);
	void load_hit(unsigned long index, unsigned long offset);
	void load_miss(unsigned long index, unsigned long tag);
	void store(unsigned long add);
	void store_hit(unsigned long index, unsigned long offset);
	void store_miss(unsigned long index, unsigned long tag);
	void eviction(unsigned long index, unsigned long tag);
	void print();

private:
	unsigned long sets;
	unsigned long blocks;
	unsigned long bytes;
	char alloc;
	char write;
	char evict;
	unsigned long lod_tot;
	unsigned long str_tot;
	unsigned long lod_hit;
	unsigned long lod_mis;
	unsigned long str_hit;
	unsigned long str_mis;
	unsigned long cyc_tot;
	unsigned long COUNTER;
	std::vector<std::vector<block>> lines;
};

#endif
