/**
 * Alice Yang
 * ayang36@jhu.edu
 * block.cpp
 */

#include "block.h"

block::block()
{
	dirty = 0;
	tag = -1;
	time = -1;
}

block::block(unsigned long t, unsigned long timer)
{
	dirty = 0;
	tag = t;
	time = timer;
}

int block::get_dirty()
{
	return dirty;
}

void block::set_dirty()
{
	dirty = 1;
}

long block::get_tag()
{
	return tag;
}

void block::set_time(unsigned long timer)
{
	time = timer;
}

unsigned long block::get_time()
{
	return time;
}
