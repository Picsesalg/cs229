/**
 * ALice Yang
 * ayang36@jhu.edu
 * block.h
 */

class block
{
    
public:
    block(int t)
    {
        tag = t;
        dirty = 0;
    }

    int get_tag();
    int get_dirty();
    void set_dirty();

private:
    int tag;
    int dirty;
};