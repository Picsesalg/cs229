/**
 * ALice Yang
 * ayang36@jhu.edu
 * block.cpp
 */

#include "block.h"

int block::get_tag()
{
    return tag;
}

int block::get_dirty()
{
    return dirty;
}

void block::set_dirty()
{
    dirty = 1;
}