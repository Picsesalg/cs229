/**
 * Alice Yang
 * ayang36@jhu.edu
 * cache.h
 */

#include <map>
#include <string>
#include <vector>

#include "block.h"

class cache
{

public:
    cache(int s, int bl, int by, std::string a, std::string w, std::string e)
    {
        sets = s;
        blocks = bl;
        bytes = by;
        if (a.compare("write-allocate") == 0) {
            alloc = 'w';
        } else {
            alloc = 'n';
        }
        if (w.compare("write-back") == 0) {
            write = 'b';
        } else {
            write = 't';
        }
        if (e.compare("lru") == 0) {
            evict = 'l';
        } else if (e.compare("fifo") == 0) {
            evict = 'f';
        } else {
            evict = 'r';
        }
        lod_tot = 0;
        str_tot = 0;
        lod_hit = 0;
        lod_mis = 0;
        str_hit = 0;
        str_mis = 0;
        cyc_tot = 0;
    }

    void load(int add);
    void load_hit(int index, int offset);
    void load_miss(int index, int tag);
    void store(int add);
    void store_hit(int index, int offset);
    void store_miss(int index, int tag);
    void print();

private:
    unsigned long sets;
    unsigned long blocks;
    unsigned long bytes;
    char alloc;
    char write;
    char evict;
    unsigned long lod_tot;
    unsigned long str_tot;
    unsigned long lod_hit;
    unsigned long lod_mis;
    unsigned long str_hit;
    unsigned long str_mis;
    unsigned long cyc_tot;
    std::map<int, std::vector<block>> cash;
};