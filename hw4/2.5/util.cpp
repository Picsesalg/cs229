/**
 * Alice Yang
 * ayang36@jhu.edu
 * util.cpp
 */

#include <ios>
#include <iostream>
#include <sstream>
#include <string>

#include "cache.h"
#include "util.h"

using std::cerr;
using std::cin;
using std::hex;
using std::istringstream;
using std::stoi;
using std::string;
using std::stringstream;

void check_arguments(int argc, char *argv[])
{
    if (argc != 6) {
		if (argc != 7) {
                	cerr << "Wrong number of arguments.\n";
                	exit(EXIT_FAILURE);
		}
    }
    if (!val_power(stoi(argv[1]), 0)) {
    	cerr << "Set number invalid.\n";
            exit(EXIT_FAILURE);
    }
    if (!val_power(stoi(argv[2]), 0)) {
        cerr << "Block number invalid.\n";
        exit(EXIT_FAILURE);
    }
    if (!val_power(stoi(argv[3]), 3)) {
        cerr << "Byte number invalid.\n";
        exit(EXIT_FAILURE);
    }
    if (string(argv[4]).compare("write-allocate") != 0) {
		if (string(argv[4]).compare("no-write-allocate") != 0) {
			cerr << "Write allocate incorrect.\n";
            exit(EXIT_FAILURE);
		}
    }
    if (string(argv[5]).compare("write-through") != 0) {
    	if (string(argv[5]).compare("write-back") != 0) {
        	exit(EXIT_FAILURE);
		}
	}
    if (argc == 7) {
     	if (string(argv[6]).compare("lru") != 0) {
			if (string(argv[6]).compare("fifo") != 0) {
                if (string(argv[6]).compare("random") != 0) {
                	cerr << "Last arg invalid.\n";
                    exit(EXIT_FAILURE);
                }
			}
		}
    }
    if (string(argv[4]).compare("no-write-allocate") == 0
        && string(argv[5]).compare("write-back") == 0) {
        	cerr << "Combination doesn't make sense.\n";
	}
	if (stoi(argv[2]) != 1 && argc != 7) {
		cerr << "Eviction policy needed.\n";
		exit(EXIT_FAILURE);
	}
	return;
}

int val_power(int arg, int min)
{
    if (arg <= min) {
                return 0;
        }

        if ((arg & (arg - 1)) == 0) {
                return 1;
        } else {
                return 0;
        }
}

void read_data(int a, int b, int c, string d, string e, string f)
{
    cache cash = cache(a, b, c, d, e, f);

    string line = "";
    string ls = "";
    string add = "";
    int address = 0;

    while (getline(cin, line)) {
        int flag = 1;
        for (unsigned int i = 0; i < line.length(); i++) {
            if (!isspace(line[i])) {
                flag = 0;
                break;
            }
        }
        if (flag) {
            continue;
        }
        istringstream iss(line);
        if (iss.eof()) {
            cerr << "Line format wrong.\n";
            exit(EXIT_FAILURE);
        }
        iss >> ls;
        val_ls(ls);
        if (iss.eof()) {
            cerr << "LIne format wrong.\n";
            exit(EXIT_FAILURE);
        }
        iss >> add;
        address = val_add(add);

        if (ls.compare("l") == 0) {
            cash.load(address);
        } else {
            cash.store(address);
        }
    }
    cash.print();
}

void val_ls(string s)
{
    if (s.compare("l") != 0) {
        if (s.compare("s") != 0) {
            cerr << "Load store wrong.\n";
            exit(EXIT_FAILURE);
        }
    }
    return;
}

int val_add(string s)
{
    if (s.substr(0, 2).compare("0x") != 0) {
        cerr << "Address wrong.\n";
        exit(EXIT_FAILURE);
    }
    s = s.substr(2);
    for (unsigned int i = 0; i < s.length(); i++) {
        if (!isxdigit(s[i])) {
            cerr << "Address wrong.\n";
            exit(EXIT_FAILURE);
        }
    }
    int add = 0;
    stringstream ss(s);
    ss << hex << s;
    ss >> add;
    if (add < 0) {
        cerr << "Address wrong.\n";
        exit(EXIT_FAILURE);
    }
    return add;
}