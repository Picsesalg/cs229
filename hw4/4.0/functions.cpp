/**
 * ALice Yang
 * ayang36@jhu.edu
 * functions.h
 */

#include "functions.h"

void cache::add (int address, char loadStore) {

    if (loadStore == 'l') {
        totalLoads++;
    } else {
        totalStores++;
    }

    //Working with given column 2 input
    address = kickOutNBits(log2(bytes), address); //takes away offset
    int addressOfSet = lowestNBits(log2(sets), address);
    address = kickOutNBits(log2(sets), address); //this is the tag

    //Introduced constants to make code easier to read
    int RAM_CYCLES = (bytes / 4) * 100;
    int CACHE_CYCLES = 1;

    universalCounter++; //tracking each add

    if (cash.find(addressOfSet) == cash.end()) { //set not found
        
        //If write allocate or load, then write to cache
        block b = block(address, universalCounter); //creates a set
        if (wa || (loadStore == 'l')) { 
            cash[addressOfSet].push_back(b);
        }

        //Not found, increment misses depending on if load or store
        if (loadStore == 's') { //store miss
            storeMisses++;
            if (wa && wtwb) {
                cycles +=  2 * RAM_CYCLES; //store to RAM
                cycles += CACHE_CYCLES;
            } else if (wa && !wtwb) {
                cycles += CACHE_CYCLES;
                b.setDirty(); //setting bit to dirty
            } else if (!wa) {
                cycles += 2 *RAM_CYCLES;
                cycles += CACHE_CYCLES;
            }
        } else {
            cycles += 2 * RAM_CYCLES; //load from RAM
            cycles += 2*CACHE_CYCLES;
            loadMisses++;
        }

    } else { //if the set is found, look for block in set

        int found = 0;
        block b = block(address, universalCounter);

        //Looping through the set vector to see if the block is found
        for (vector<block>::iterator it = cash[addressOfSet].begin();
            it != cash[addressOfSet].end(); ++it) {

            int j = it->findTag();
            //cout << j << endl;

            if (j == address) {

                //Found in vector
                if (loadStore == 's') {
                    storeHits++;
                    cycles += CACHE_CYCLES;
                    if (wtwb) {
                        cycles += RAM_CYCLES;
                    } else {
                        it->setDirty();
                    }
                } else {
                    loadHits++;
                    cycles += 2 * CACHE_CYCLES;
                }

                found = 1;

                it->updateEvict(universalCounter);
                break;
            }
        }


        if (!found) {
            //Tag not found in set, increment misses
            if (loadStore == 's') { //store miss
                storeMisses++;
                if (wa && wtwb) {
                    cycles +=  2 * RAM_CYCLES; //store to RAM
                    cycles += CACHE_CYCLES;
                } else if (wa && !wtwb) {
                    cycles += CACHE_CYCLES;
                    b.setDirty(); //setting bit to dirty
                } else if (!wa) {
                    cycles += 2 * RAM_CYCLES;
                    cycles += CACHE_CYCLES;
                }
            } else {
                cycles += 2 * RAM_CYCLES; //load from RAM
                cycles += 2 * CACHE_CYCLES;
                loadMisses++;
            }


            //Add tag to existing set if write allocate or load
            if (wa || (loadStore == 'l')) {
                block b = block(address, universalCounter);
                cash[addressOfSet].push_back(b);
                if (!wtwb && loadStore == 's') {
                    b.setDirty();
                }
            }


            if (cash[addressOfSet].size() == (blocks + 1)) {
                if (!lrufifo) { //FIFO

                    //Check if dirty bit is set
                    if ((cash[addressOfSet].front()).isDirty()) {
                        //If dirty, then must store to RAM before erasing
                        cycles += RAM_CYCLES;
                    }

                    cash[addressOfSet].erase(cash[addressOfSet].begin());
                } else { //LRU
                    //Minimum value of evict counter
                    int min = cash[addressOfSet].begin()->findEvictCount();

                    //Iterate through the vector of blocks (the set)
                    vector<block>::iterator toRemove = cash[addressOfSet].begin();

                    //Loop through and set minimum evict count
                    for (vector<block>::iterator iter = cash[addressOfSet].begin(); iter != cash[addressOfSet].end(); ++iter) {
                        if (min > iter->findEvictCount()) {
                            toRemove = iter;
                            min = iter->findEvictCount();
                        }
                    }

                    if (toRemove->isDirty()) {
                        cycles += RAM_CYCLES;
                    }

                    //Erase at iterator value
                    cash[addressOfSet].erase(toRemove);
                }
            }
        }
    }

}

void cache::printFunction () {
    cout << "Total loads: " << (int) totalLoads << endl;
    cout << "Total stores: " << (int) totalStores << endl;
    cout << "Load hits: " << (int) loadHits << endl;
    cout << "Load misses: " << (int) loadMisses << endl;
    cout << "Store hits: " << (int) storeHits << endl;
    cout << "Store misses: " << (int) storeMisses << endl;
    cout << "Total cycles: " << (int) cycles << endl;
}
