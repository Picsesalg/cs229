/**
 * Alice Yang
 * ayang36@jhu.edu
 * functions.h
 */

#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <iostream>
#include <iomanip>
#include <map>
#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <math.h>
#include <vector>
#include <algorithm>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::hex;
using std::fixed;
using std::setprecision;
using std::map;
using std::pair;
using std::vector;

class block {

public:
    block(int t, int ec) {
        tag = t;
        dirtyBit = 0;
        evictCount = ec;
    }

    //functions
    int findTag() {
        return tag;
    }
    int updateEvict(int i) {
        evictCount = i;
        return evictCount;
    }
    int findEvictCount() {
        return evictCount;
    }
    int isDirty() {
        return dirtyBit;
    }
    void setDirty() {
        dirtyBit = 1;
    }

private:
    int tag;
    int dirtyBit;
    int evictCount;
};

class cache {

public:
	cache(int s, int bl, int by, std::string wra, std::string tb, std::string lf) {
        sets = s;
        blocks = bl;
        bytes = by;
        if (wra.compare("write-allocate") == 0) {
            wa = 1;
        } else {
            wa = 0;
        }
        if (tb.compare("write-back") == 0) {
            wtwb = 0;
        } else {
            wtwb = 1;
        }
        if (lf.compare("lru") == 0) {
            lrufifo = 1;
        } else if (lf.compare("fifo") == 0) {
            lrufifo = 0;
        } else {
            lrufifo = 2;
        }
        totalLoads = 0;
        totalStores = 0;
        loadHits = 0;
        loadMisses = 0;
        storeHits = 0;
        storeMisses = 0;
        cycles = 0;
        universalCounter = 0;
    }

	void printFunction();
    void add(int address, char loadStore);
    int lowestNBits(int n, int start) {
        int mask;
        mask = (1 << n) - 1; //returns lowest n bits by masking
        return start & mask;
    }
    int kickOutNBits(int n, int add) {
        return (add >> n);
    }

private:
    int sets;
    int blocks;
    int bytes;
    int wa;
    int wtwb;
    int lrufifo;
    double totalLoads;
    double totalStores;
    double loadHits;
    double loadMisses;
    double storeHits;
    double storeMisses;
    double cycles;
    int universalCounter;
    map<int, vector<block> > cash;
};

#endif