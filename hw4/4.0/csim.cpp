/**
 * Alice Yang
 * ayang36@jhu.edu
 * csim.cpp
 */

#include <string>

#include "util.h"

using std::stoi;
using std::string;

int main(int argc, char *argv[])
{
    check_arguments(argc, argv);

    int sets = stoi(argv[1]);
    int blocks = stoi(argv[2]);
    int bytes = stoi(argv[3]);
    string alloc = string(argv[4]);
    string write = string(argv[5]);
    string evict = string(argv[6]);

    read_data(sets, blocks, bytes, alloc, write, evict);

    exit(EXIT_SUCCESS);
}