/**
 * Alice Yang
 * ayang36@jhu.edu
 * read.cpp
 */

#include <iostream>
#include <string>

#include "read.hpp"

using std::cerr;
using std::string;
using std::stoi;

void check_arguments(int argc, char *argv[]) {
	if (argc != 6 || argc != 7) {
                cerr << "Wrong number of arguments.\n";
                exit(EXIT_FAILURE);
        }
        if (!val_power(stoi(argv[1]), 0)) {
                cerr << "Set number invalid.\n";
                exit(EXIT_FAILURE);
        }
        if (!val_power(stoi(argv[2]), 0)) {
                cerr << "Block number invalid.\n";
                exit(EXIT_FAILURE);
        }
        if (!val_power(stoi(argv[3]), 4)) {
                cerr << "Byte number invalid.\n";
                exit(EXIT_FAILURE);
        }
        if (string(argv[4]).compare("write-allocate") != 0
                || string(argv[4]).compare("no-write-allocate") != 0) {
                cerr << "Write allocate incorrect.\n";
                exit(EXIT_FAILURE);
        }
        if (string(argv[5]).compare("write-through") != 0
                || string(argv[5]).compare("write-back") != 0) {
                cerr << "Write-back/through incorrect.\n";
                exit(EXIT_FAILURE);
        }
        if (argc == 7) {
                if (string(argv[6]).compare("lru") != 0
                        || string(argv[6]).compare("fifo") != 0
                        || string(argv[6]).compare("random") != 0) {
                        cerr << "Last arg invalid.\n";
                        exit(EXIT_FAILURE);
                }
        }
        if (string(argv[4]).compare("no-write-allocate") == 0
                && string(argv[5]).compare("write-back") == 0) {
                cerr << "Combination doesn't make sense.\n";
                exit(EXIT_FAILURE);
        }
        if (stoi(argv[2]) == 1 && argc == 7) {
                cerr << "Direct mapped has no eviction policy.\n";
                exit(EXIT_FAILURE);
        }		
}

int val_power(int arg, int min) {
	if (arg < min) {
		return 0;
	}
	if ((arg & (arg - 1)) == 0) {
		return 1;
	} else {
		return 0;
	}
}
