/**
 * Alice Yang
 * ayang36@jhu.edu
 * csim.cpp
 */

#include <iostream>
#include <string>
#include <sstream>

#include "read.hpp"
#include "Cache.hpp"

using std::cerr;
using std::cin;
using std::endl;
using std::stoi;
using std::string;
using std::istringstream;
using std::hex;
using std::stringstream;

/*int val_set(char *argv[]);
int val_block(char *argv[]);
int val_byte(char *argv[]);*/
int val_add(string add);
void read_data(int s, int bl, int by, string a, string w, string e);

int main(int argc, char *argv[]) {
	
	/*if (argc != 6 || argc != 7) {
        	cerr << "Wrong number of arguments.\n";
        	return EXIT_FAILURE;
    	}
    	if (!val_set(argv)) {
        	cerr << "Set number invalid.\n";
        	return EXIT_FAILURE;
    	}
    	if (!val_block(argv)) {
        	cerr << "Block number invalid.\n";
        	return EXIT_FAILURE;
    	}
    	if (!val_byte(argv)) {
        	cerr << "Byte number invalid.\n";
        	return EXIT_FAILURE;
    	}
    	if (string(argv[4]).compare("write-allocate") != 0
        	|| string(argv[4]).compare("no-write-allocate") != 0) {
        	cerr << "Write allocate incorrect.\n";
        	return EXIT_FAILURE;
    	}
    	if (string(argv[5]).compare("write-through") != 0
        	|| string(argv[5]).compare("write-back") != 0) {
        	cerr << "Write-back/through incorrect.\n";
        	return EXIT_FAILURE;
    	}
	if (argc == 7) {
		if (string(argv[6]).compare("lru") != 0
			|| string(argv[6]).compare("fifo") != 0
			|| string(argv[6]).compare("random") != 0) {
			cerr << "Last arg invalid.\n";
			return EXIT_FAILURE;
		}
	}
	if (string(argv[4]).compare("no-write-allocate") == 0
		&& string(argv[5]).compare("write-back") == 0) {
		cerr << "Combination doesn't make sense.\n";
		return EXIT_FAILURE;
	}
	if (stoi(argv[2]) == 1 && argc == 7) {
		cerr << "Direct mapped has no eviction policy.\n";
		return EXIT_FAILURE;
	}*/

	check_arguments(argc, argv);

	int sets = stoi(argv[1]);
	int blocks = stoi(argv[2]);
	int bytes = stoi(argv[3]);
	string allocate = string(argv[4]);
	string write = string(argv[5]);
	string eviction = "";

	if (argc == 7) {
		eviction = string(argv[6]);
	}
	
	read_data(sets, blocks, bytes, allocate, write, eviction);

    	return EXIT_SUCCESS;
}

/*int val_set(char *argv[]) {
    	int set = stoi(argv[1]);
    	if (set <= 0) {
        	return 0;
    	}
    	if ((set & (set - 1)) == 0) {
        	return 1;
    	} else {
        	return 0;
    	}
}

int val_block(char *argv[]) {
    	int block = stoi(argv[2]);
    	if (block <= 0) {
        	return 0;
    	}
    	if ((block & (block - 1)) == 0) {
        	return 1;
    	} else {
        	return 0;
    	}
}

int val_byte(char *argv[]) {
    	int byte = stoi(argv[3]);
    	if (byte < 4) {
        	return 0;
    	}
    	if ((byte & (byte - 1)) == 0) {
        	return 1;
    	} else {
        	return 0;
    	}
}*/

int val_add(string add) {
	for (unsigned int i = 0; i < add.length(); i++) {
		if (!isxdigit(add[i])) {
			return -1;
		}
	}
	int address = 0;
	stringstream ss(add);
	ss << hex << add;
	ss >> address;
	if (address < 0) {
		return -1;
	}
	return address;
}

void read_data(int s, int bl, int by, string a, string w, string e) {
	
	string line = "";
	string ls = "";
	string add = "";

	if (a.compare("write-allocate") == 0) {
		a = "wa";
	} else if (a.compare("no-write-allocate") == 0) {
		a = "nwa";
	}
	/*switch(a) {
		case "write-allocate":
			a = "wa";
			break;
		case "no-write-allocate":
			a = "nwa";
			break;
		default:
			break;
	}*/

	if (w.compare("write-through") == 0) {
		w = "wt";
	} else if (w.compare("no-write-through") == 0) {
		w = "nwt";
	}
	/*switch(w) {
		case "write-through":
			w = "wt";
			break;
		case "write-back":
			w = "wb";
			break;
		default:
			break;
	}*/

	Cache cache = Cache(s, bl, by, a, w, e);

	while (getline(cin, line)) {
		int flag = 1;
		for (unsigned int i = 0; i < line.length(); i++) {
			if (!isspace(line[i])) {
				flag = 0;
				break;
			}
		}
		if (flag) {
			continue;
		}
		istringstream iss(line);
		if (iss.eof()) {
			cerr << "Line incorrect.\n";
			exit(EXIT_FAILURE);
		}
		iss >> ls;
		if (ls.compare("l") != 0 || ls.compare("s") != 0) {
			cerr << "Line incorrect.\n";
			exit(EXIT_FAILURE);
		}
		if (iss.eof()) {
			cerr << "Line incorrect.\n";
			exit(EXIT_FAILURE);
		}
		iss >> add;
		if (add.substr(0, 2).compare("0x") != 0) {
			cerr << "Line incorrect.\n";
			exit(EXIT_FAILURE);
		}
		if (val_add(add.substr(2)) == -1) {
			cerr << "Line incorrect.\n";
			exit(EXIT_FAILURE);
		}
		if (ls.compare("l") == 0) {
			cache.inc_lod(val_add(add.substr(2)));
		} 
	}
}
