/**
 * Alice Yang
 * ayang36@jhu.edu
 * ayang36@jhu.edu
 */

#include <map>
#include <string>
#include <vector>

using std::map;
using std::string;
using std::vector;

class Block {

public:
	Block(int t, int a) {
		dirty = 0;
		valid = 1;
		tag = t;
		age = a;
		use = 1;
	}

	int getDirty() {
		return dirty;
	}

	int getTag() {
		return tag;
	}

	int getAge() {
		return age;
	}

	int getUse() {
		return use;
	}

	void updateUse() {
		use++;
	}

private:
	int dirty;
	int valid;
	int tag;
	int age;
	int use;
};

class Cache {

public:
	Cache(int s, int bl, int by, string a, string b, string c) {
		sets = s;
		blocks = bl;
		bytes = by;
		wanwa = a;
		wtwb = b;
		evict = c;
		lod_tot = 0;
		str_tot = 0;
		lod_hit = 0;
		lod_mis = 0;
		str_hit = 0;
		str_mis = 0;
		cyc_tot = 0;
		COUNTER = 0;
	}

	void inc_lod(int address);
	void inc_str(int address);
	void eviction(vector<Block> v, Block);
	string toString();

private:
	int sets;
	int blocks;
	int bytes;
	string wanwa;
	string wtwb;
	string evict;
	int lod_tot;
	int str_tot;
	int lod_hit;
	int lod_mis;
	int str_hit;
	int str_mis;
	int cyc_tot;
	int COUNTER;
	map<int, vector<Block>> lines;
};
