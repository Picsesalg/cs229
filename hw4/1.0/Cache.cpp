/**
 * Alice Yang
 * ayang36@jhu.edu
 * Cache.cpp
 */

#include <cmath>
#include <cstdlib>
#include <map>
#include <string>
#include <vector>

#include "Cache.hpp"

using std::log2;
using std::pair;
using std::string;
using std::vector;

void Cache::inc_lod(int address) {
	
	COUNTER++;

	lod_tot++;
	
	/* Breaking up the address. */
	//int offset = address & (bytes - 1);
	int temp = log2(bytes);
	address = address >> temp;
	int index = address & (sets - 1);
	temp = log2(sets);
	int tag = address >> temp;

	/* The index is empty. */
	if (lines.find(index) == lines.end()) {
		lod_mis++;
		cyc_tot++;
		cyc_tot += bytes / 4 * 100;
		Block b = Block(tag, COUNTER);
		lines[index].push_back(b);
	} else { /* The index is valid. */
		vector<Block> v = lines[index];

		/* Check if the tag is valid. */
		int flag = 1;
		for (int i = 0; i < v.size(); i++) {
			/* Tag is valid in the set. */
			if (v[i].getTag() == tag) {
				lod_hit++;
				cyc_tot++;
				v[i].updateUse();
				flag = 0;
				break;
			}
		}
		/* Tag not valid in the set. */
		if (flag) {
			lod_mis++;
			Block b = Block(tag, COUNTER);
			Cache::eviction(v, b);
			cyc_tot++;
		}
	}
}

void Cache::inc_str(int address) {
	
	COUNTER++;

	str_tot++;
	
	int temp = log2(bytes);
	address = address >> temp;
	int index = address & (sets - 1);
	temp = log2(sets);
	int tag = address >> temp;

	/* If the index is empty. */
	if (lines.find(index) == lines.end()) {
		str_mis++;
	}
}

void Cache::eviction(vector<Block> v, Block b) {
	/* Set not full. */
	if (v.size() != blocks) {
		cyc_tot = bytes / 4 * 100;
		v.push_back(b);
	} else if (evict.compare("lru") == 0) { //TODO clarify lru policy 
		int min = v[0].getUse();
		int ind = 0;
		for (int i = 1; i < v.size(); i++) {
			if (v[i].getUse() < min) {
				min = v[i].getUse();
				ind = i;
			}
		}
		cyc_tot = bytes / 4 * 100;
		v[ind] = b;
	} else if (evict.compare("fifo") == 0) {/* FIFO policy. */
		int min = v[0].getAge();
		int ind = 0;
		for (int i = 1; i < v.size(); i++) {
			if (v[i].getAge() < min) {
				min = v[i].getAge();
				ind = i;
			}
		}
		cyc_tot = bytes / 4 * 100;
		v[ind] = b;
	} else if (evict.compare("random") == 0) { /* Random policy. */
		int ind = rand() % blocks;
		cyc_tot = bytes / 4 * 100;
		v[ind] = b;
	} else { /* No policy. So direct mapped. */
		cyc_tot = bytes / 4 * 100;
		v[0] = b;
	}
}

string Cache::toString() {
	string s = "Total loads:";
	s += lod_tot;
	s += "\nTotal stores: ";
	s += str_tot;
	s += "\nLoad hits: ";
	s += lod_hit;
	s += "\nLoad misses: ";
	s += lod_mis;
	s += "\nStore hits: ";
	s += str_hit;
	s += "\nStore misses: ";
	s += str_mis;
	s += "\nTotal cycles: ";
	s += cyc_tot;
	s += "\n";
	return s;
}
