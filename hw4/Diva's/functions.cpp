/*
Name: Diva Parekh
Email: dparekh2@jhu.edu
600.229 Assignment 5, Problem 1
File: functions.cpp
*/

#include "functions.hpp"

void Cache::add (int address, char loadStore) {

    if (loadStore == 'l') {
        totalLoads++;
    } else {
        totalStores++;
    }

    //Working with given column 2 input
    address = kickOutNBits(log2(bytes), address); //takes away offset
    int addressOfSet = lowestNBits(log2(sets), address);
    address = kickOutNBits(log2(sets), address); //this is the tag

    //Introduced constants to make code easier to read
    int RAM_CYCLES = (bytes / 4) * 100;
    int CACHE_CYCLES = 1;

    universalCounter++; //tracking each add

    if (cache.find(addressOfSet) == cache.end()) { //set not found
        
        //If write allocate or load, then write to cache
        Block b = Block(address, universalCounter); //creates a set
        if (wa || (loadStore == 'l')) { 
            cache[addressOfSet].push_back(b);
        }

        //Not found, increment misses depending on if load or store
        if (loadStore == 's') { //store miss
            storeMisses++;
            if (wa && wtwb) {
                cycles +=  2 * RAM_CYCLES; //store to RAM
                cycles += CACHE_CYCLES;
            } else if (wa && !wtwb) {
                cycles += CACHE_CYCLES;
                b.setDirty(); //setting bit to dirty
            } else if (!wa) {
                cycles += 2 *RAM_CYCLES;
                cycles += CACHE_CYCLES;
            }
        } else {
            cycles += 2 * RAM_CYCLES; //load from RAM
            cycles += 2*CACHE_CYCLES;
            loadMisses++;
        }

    } else { //if the set is found, look for block in set

        int found = 0;
        Block b = Block(address, universalCounter);

        //Looping through the set vector to see if the block is found
        for (vector<Block>::iterator it = cache[addressOfSet].begin();
            it != cache[addressOfSet].end(); ++it) {

            int j = it->findTag();
            //cout << j << endl;

            if (j == address) {

                //Found in vector
                if (loadStore == 's') {
                    storeHits++;
                    cycles += CACHE_CYCLES;
                    if (wtwb) {
                        cycles += RAM_CYCLES;
                    } else {
                        it->setDirty();
                    }
                } else {
                    loadHits++;
                    cycles += 2 * CACHE_CYCLES;
                }

                found = 1;

                it->updateEvict(universalCounter);
                break;
            }
        }


        if (!found) {
            //Tag not found in set, increment misses
            if (loadStore == 's') { //store miss
                storeMisses++;
                if (wa && wtwb) {
                    cycles +=  2 * RAM_CYCLES; //store to RAM
                    cycles += CACHE_CYCLES;
                } else if (wa && !wtwb) {
                    cycles += CACHE_CYCLES;
                    b.setDirty(); //setting bit to dirty
                } else if (!wa) {
                    cycles += 2 * RAM_CYCLES;
                    cycles += CACHE_CYCLES;
                }
            } else {
                cycles += 2 * RAM_CYCLES; //load from RAM
                cycles += 2 * CACHE_CYCLES;
                loadMisses++;
            }


            //Add tag to existing set if write allocate or load
            if (wa || (loadStore == 'l')) {
                Block b = Block(address, universalCounter);
                cache[addressOfSet].push_back(b);
                if (!wtwb && loadStore == 's') {
                    b.setDirty();
                }
            }


            if (cache[addressOfSet].size() == (blocks + 1)) {
                if (!lrufifo) { //FIFO

                    //Check if dirty bit is set
                    if ((cache[addressOfSet].front()).isDirty()) {
                        //If dirty, then must store to RAM before erasing
                        cycles += RAM_CYCLES;
                    }

                    cache[addressOfSet].erase(cache[addressOfSet].begin());
                } else { //LRU
                    //Minimum value of evict counter
                    int min = cache[addressOfSet].begin()->findEvictCount();

                    //Iterate through the vector of blocks (the set)
                    vector<Block>::iterator toRemove = cache[addressOfSet].begin();

                    //Loop through and set minimum evict count
                    for (vector<Block>::iterator iter = cache[addressOfSet].begin(); iter != cache[addressOfSet].end(); ++iter) {
                        if (min > iter->findEvictCount()) {
                            toRemove = iter;
                            min = iter->findEvictCount();
                        }
                    }

                    if (toRemove->isDirty()) {
                        cycles += RAM_CYCLES;
                    }

                    //Erase at iterator value
                    cache[addressOfSet].erase(toRemove);
                }
            }
        }
    }

}

void Cache::printFunction () {
    cout << "Total loads: " << (int) totalLoads << endl;
    cout << "Total stores: " << (int) totalStores << endl;
    cout << "Load hits: " << (int) loadHits << endl;
    cout << "Load misses: " << (int) loadMisses << endl;
    cout << "Store hits: " << (int) storeHits << endl;
    cout << "Store misses: " << (int) storeMisses << endl;
    cout << "Total cycles: " << (int) cycles << endl;
}
