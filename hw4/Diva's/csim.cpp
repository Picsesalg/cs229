/*
    Name: Diva Parekh
    Email: dparekh2@jhu.edu
    600.229 Assignment 5, Problem 1
    File: csim.cpp
*/

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <cctype>
#include <math.h>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <iomanip>
#include "functions.hpp"

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::istringstream;
using std::ifstream;
using std::hex;

int isPowerOfTwo (int n);
int is0or1 (int n);
int stringToHex (string str);

int main(int argc, char * argv[]) {

    if (argc != 8) {
        cerr << "Invalid arguments" << endl;
        return EXIT_FAILURE;
    }

    //Taking in command line input
    int numSets = atoi(argv[1]);
    int numBlocks = atoi(argv[2]);
    int bytesPerBlock = atoi(argv[3]);
    int isWA = atoi(argv[4]);
    int WTorWB = atoi(argv[5]);
    int lruOrFIFO = atoi(argv[6]);
    string fileName = string(argv[7]);

    //Checking validity of command line input
    /*if (!isPowerOfTwo(numSets)) {
        cerr << "Invalid number of sets" << endl;
        return EXIT_FAILURE;
    }

    if (!isPowerOfTwo(numBlocks)) {
        cerr << "Invalid number of blocks" << endl;
        return EXIT_FAILURE;
    }

    if (!isPowerOfTwo(bytesPerBlock) || (bytesPerBlock < 4)) {
        cerr << "Invalid number of bytes in each block" << endl;
        return EXIT_FAILURE;
    }

    if (!is0or1(isWA) || !is0or1(WTorWB) || !is0or1(lruOrFIFO)) {
        cerr << "Invalid input" << endl;
        return EXIT_FAILURE;
    }*/

    //Initializing variables to store data read in from the file
    char loadStore = ' ';
    int address = 0;
    int junk = 0;
    string addStr = " ";
    int lineCount = 0;

    //ifstream file;
    //file.open(fileName.c_str());

    /*if (!isWA && !WTorWB) {
        cerr << "No write allocate and write back are incompatible" << endl;
        return EXIT_FAILURE;
    }*/

    Cache cashMoney = Cache(numSets, numBlocks, bytesPerBlock, isWA, WTorWB, lruOrFIFO);

    //if (file.is_open()) {
        while (std::cin >> loadStore >> addStr >> junk) {
            if ((loadStore != 'l') && (loadStore != 's')) {
                cerr << "Invalid character" << endl;
                return EXIT_FAILURE;
            }
            address = stringToHex(addStr);
            cashMoney.add(address, loadStore);
            /*if (cashMoney.printCache() == 1) {
                cout << "line: " << lineCount << endl;
            }*/
        }
    /*} else {
        cerr << "Failed to open file" << endl;
        return EXIT_FAILURE;*/
    //}
    //gonna delete printCache function later - want to print final cache
    //cashMoney.printCache();
    cashMoney.printFunction();
    //file.close();
    return EXIT_SUCCESS;
}


int isPowerOfTwo (int n) {
    if (n == 0) {
        return 0;
    }

    if (!(n & (n - 1))) {
        return 1;
    }
    
    return 0;
}

int is0or1 (int n) {
    if ((n == 0) || (n == 1)) {
        return 1;
    }
    return 0;
}

int stringToHex (string str) {
    int toReturn = 0;
    istringstream toHex(str);
    toHex >> hex >> toReturn;
    return toReturn;
}
