/*
Name: Diva Parekh
Email: dparekh2@jhu.edu
600.229 Assignment 5, Problem 1
File: functions.hpp
*/

#ifndef FUNCTIONS_HPP
#define FUNCTIONS_HPP

#include <iostream>
#include <iomanip>
#include <map>
#include <string>
#include <cstring>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include <utility>
#include <math.h>
#include <vector>
#include <algorithm>

using std::string;
using std::cin;
using std::cout;
using std::cerr;
using std::endl;
using std::hex;
using std::fixed;
using std::setprecision;
using std::map;
using std::pair;
using std::vector;

class Block {

public:
    Block(int t, int ec) {
        tag = t;
        dirtyBit = 0;
        evictCount = ec;
    }

    //functions
    int findTag() {
        return tag;
    }
    int updateEvict(int i) {
        evictCount = i;
    }
    int findEvictCount() {
        return evictCount;
    }
    int isDirty() {
        return dirtyBit;
    }
    void setDirty() {
        dirtyBit = 1;
    }

private:
    int tag;
    int dirtyBit;
    int evictCount;
};



class Cache {

public:
	Cache(int s, int bl, int by, int wra, int tb, int lf) {
        sets = s;
        blocks = bl;
        bytes = by;
        wa = wra;
        wtwb = tb;
        lrufifo = lf;
        totalLoads = 0;
        totalStores = 0;
        loadHits = 0;
        loadMisses = 0;
        storeHits = 0;
        storeMisses = 0;
        cycles = 0;
        universalCounter = 0;
    }
	void printFunction();
    void add(int address, char loadStore);
    int lowestNBits(int n, int start) {
        int mask;
        mask = (1 << n) - 1; //returns lowest n bits by masking
        return start & mask;
    }
    int kickOutNBits(int n, int add) {
        return (add >> n);
    }
private:
    int sets;
    int blocks;
    int bytes;
    int wa;
    int wtwb;
    int lrufifo;
    double totalLoads;
    double totalStores;
    double loadHits;
    double loadMisses;
    double storeHits;
    double storeMisses;
    double cycles;
    int universalCounter;
    map<int, vector<Block> > cache;
};

#endif