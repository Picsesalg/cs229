# Alice Yang
# ayang36@jhu.edu
# hex.S

SYS_READ = 0
SYS_WRITE = 1
SYS_EXIT = 60

STDIN_FILENO = 0
STDOUT_FILENO = 1
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

	.text
	.globl	_start

_start:
	movl	$0, %eax		# int position = 0
	movl	$0, %esi		# int counter = 0; To format 8 chars

_position_calc:
	cmp	$8, %esi		# Compare counter with 8
	je	_position_print		# If 8 chars are printed, then go to printing ascii values

	movl	$0, %edx		# For word long division
	movl	$16, %ebx		# Divisor = 16
	divl	%ebx			# Position div 16
	addl	$48, %edx		# Ascii value of the remainder
	push	%rdx			# Push digit onto stack
	
	incl	%esi			# counter++
	jmp	_position_calc

_position_print:
	cmp	$0, %esi		# Compare counter with 0
	je	_position_done		# If counter == 0, start start reading chars

	movq	$SYS_WRITE, %rax	# write() system call
	movq	$STDOUT_FILENO, %rdi	# Write to stdout
	pop	%rcx
	lea	(%rcx), %rsi		# The address of the pointer
	movq	$4, %rdx		# Write 4 bytes
	syscall

	addq	$4, %rsp		# Pop
	decl	%esi			# counter--
	jmp	_position_print

_position_done:
	movq    $SYS_WRITE, %rax        # write() system call
	movq    $STDOUT_FILENO, %rdi    # Write to stdout
	leaq    colon(%rip), %rsi       # The address of the pointer
 	movq    $lengthc, %rdx		# Write colon bytes
 	syscall

	movl	$0, %esi		# counter = 0

_char_loop:
	cmpl	$16, %esi		# Compare counter with 16
	je	_exit_success

/*	movq	$lengths, %rdx
	movq    $SYS_WRITE, %rax        # write() system call
        movq    $STDOUT_FILENO, %rdi    # Write to stdout
        leaq    space(%rip), %rsi       # The address of the pointer
        syscall*/

	incl	%esi			# counter++
	jmp	_char_loop

_exit_success:
	mov     $SYS_EXIT, %rax         # exit() system call
        mov     $EXIT_SUCCESS, %rdi     # exit status to return to shell
        syscall

_exit_failure:
	mov	$SYS_EXIT, %rax		# exit() sys call
	mov	$EXIT_FAILURE, %rdi	# fail status return to shell
	syscall

	.data
buffer:
	.space	1

colon:
	.asciz	": "

space:
	.asciz	" "

lengthb = . - buffer

lengthc = . - colon

lengths = . - space
