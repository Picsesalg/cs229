#include <stdio.h>
#include <stdlib.h>

// first 6 go into registers, left to right, remaining on stack, right to left
int func(int a, int b, int c, int d, int e, int f, int g, int h, int i)
{
	return a + b + c + d + e + f + g + h + i;
}

int main(void)
{
	printf("hi!\n");
	func(1, 2, 3, 4, 5, 6, 7, 8, 9);
	return EXIT_SUCCESS;
}
