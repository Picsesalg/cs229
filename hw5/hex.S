# Alice Yang
# ayang36@jhu.edu
# hex.S

SYS_READ = 0
SYS_WRITE = 1
SYS_EXIT = 60

STDIN_FILENO = 0
STDOUT_FILENO = 1
EXIT_SUCCESS = 0
EXIT_FAILURE = 1

	.text
	.globl	_start

# prints a character as the character or a dot
# void _char(int input)
_char:
	pushq	%rbp			# push the address of previous stack
	movq	%rsp, %rbp		# store address of current stack frame
	movl	%edi, -0x4(%rbp)		# save the ascii of the input

	movl	$0x1, %edx		# 1 byte to print
	movl	$SYS_WRITE, %eax	# write() call
	movl	$STDOUT_FILENO, %edi	# to stdout
	cmpl	$0x20, -0x4(%rbp)		# compare input with ascii of SPACE
	jl	char_dot		# if input < 32, print .
	jmp	char_norm		# else print the char

char_dot:
	leal	dot(%rip), %esi		# address of dot in rsi
	jmp	char_exit		# exit

char_norm:
	leaq	-0x4(%rbp), %rsi	# address of input ascii in rsi	
	
char_exit:
	syscall				# print
	leave				# pop
	ret				# return

# converts input to its ascii in hex
# int _to_ascii(int input)
_to_ascii:
	push	%rbp			# store return address and the like
	mov	%rsp, %rbp		# old rbp to where it's held
	
	cmpl	$0xa, %edi		# compare digit with 10
	jl	digit			# if input < 10, it's a numerical digit
	jmp	letter			# else, it's a lower case letter

digit:
	addl	$0x30, %edi		# add 48 for a numerical digit
	jmp	to_ascii_exit		# leave

letter:
	addl	$0x57, %edi		# add 97 - 10 for lower case letter

to_ascii_exit:
	movl	%edi, %eax		# return new ascii value

	leave
	ret

# prints the value to the length of format
# void _calc(int value, int format)
_calc:
	pushq	%rbp			# store return address
	movq	%rsp, %rbp		# point old rbp to its location on stack
	subq	$0x10, %rsp		# assign 
	movl	%edi, -0x4(%rbp)	# value to be printed
	movl	%esi, -0x8(%rbp)	# num of chars to be printed

	movl	-0x4(%rbp), %edx	# the value to be printed
	movl	$0x0, %ebx		# counter == 0

calc_calculation:
        incl    %ebx                    # counter++
        movl    %edx, %ecx              # digit to be gotten in %ecx
        andl    $0xf, %ecx              # digit = last 4 bits
        movl    %ecx, %edi              # get the ascii for this
        call    _to_ascii
        push    %rax                    # Push this digit to stack
        shr     $0x4, %edx              # shift position 4 bits right
        cmpl    %esi, %ebx
        je      calc_loop2        	# counter = 8, leave
        jmp     calc_calculation       	# else keep going

calc_loop2:
        decl    %ebx                    # counter--
        movl    $0x1, %edx              # 1 byte to print for char
        movl    $SYS_WRITE, %eax        # write()
        movl    $STDOUT_FILENO, %edi    # to stdout
        leaq    (%rsp), %rsi            # print stack
        syscall				# syscall
        addq    $0x8, %rsp		# pop the stack
        cmpl    $0x0, %ebx		# compare counter with 0.
        je      calc_loop2_exit		# if counter = 0, leave
        jmp     calc_loop2              # else keep going

calc_loop2_exit:
	movl	-8(%rbp), %esi		# length of format into esi
	cmpl	$0x2, %esi		# if format == 2, print space
	je	calc_char

calc_pos:
	movl	$lengthc, %edx		# length of colon space
	leal	colon(%rip), %esi	# address of colon
	syscall
	jmp	calc_exit		# leave

calc_char:
	movl	$lengths, %edx		# length of a space
	leaq	space(%rip), %rsi	# address of space
	syscall

calc_exit:
	leave
	ret

# int _read()
# returns the length of input/syscall return value
_read:
	push	%rbp
	movq	%rsp, %rbp
	subq	$0x10, %rsp

	movl	$0x0, %r10d		# i = 0

	movl	$STDIN_FILENO, %edi	# from stdin
	movl	$lengtho, %edx		# 1 byte char to read in

read_loop1:
	movl	$SYS_READ, %eax		# read() call
	leal	one_char(%rip), %esi	# address of stack pointer
	syscall

	movl	%eax, %r15d		# syscall return value
	test	%eax, %eax		# test against 0
	je	read_exit		# if ctrl-d, then exit
	jl	start_error		# <0, is an error

	movb	one_char(%rip), %al	# the value of input char in al

	leaq	buffer(%rip), %r11	# address of buffer in r11
	addl	%r10d, %r11d		# buffer[i]
	movb	%al, (%r11)		# the value of input in buffer[i]

	incl	%r10d			# i++
	cmpl	$0x10, %r10d		# compare i with 16
	je	read_exit		# i == 16, stop reading chars
	jmp	read_loop1

read_exit:
	movl	%r10d, %eax		# return length of buffer
	leave
	ret

_start:
	movl	$0x0, %r8d		# position == 0
	movl	$0x1, %r15d		# syscall return to check whether end of input is read

read_loop:
	cmpl	$0x0, %r15d		# check syscall value
	je	start_exit		# if end of input is read previously, exit
#	jl	start_error

	call	_read

	movl	%eax, %r9d		# length of buffer input

        testl   %eax, %eax
        je      start_exit		# eof

	movl	%r8d, %edi		# position is 1st arg
	movl	$0x8, %esi		# 8 chars to print out
	call	_calc
	addl	$0x10, %r8d		# position++

	movl	$0x0, %r10d		# i = 0  to iterate through buffer

start_loop1:
	leaq	buffer(%rip), %r11	# move address of buffer into r11
	addl	%r10d, %r11d		# buffer address + i
	movb	(%r11), %dil		# 1 byte of buffer[i] is 1st arg
	movl	$0x2, %esi		# 2 chars to print 2nd arg
	call	_calc			# perform hex calc and print
	incl	%r10d			# i++
	cmpl	%r9d, %r10d		# compare i with length of input
	je	start_loop1_exit	# if i == length, leave
	jmp	start_loop1		# if i < length, repeat

start_loop1_exit:
#	movl	$0x0, %edx
	imul	$-0x3, %r9d		# input length * -3
	addl	$0x31, %r9d		# 48 + -input length for the amount
					# of spaces to account for format
	movl	$0x0, %r10d		# i = 0

	movl    $lengths, %edx          # size of space
        movl    $STDOUT_FILENO, %edi    # to stdout
        leal    space(%rip), %esi       # address of space

start_loop2:
	cmpl	%r9d, %r10d		# compare i with number of times to print
	je	start_loop2_exit	# i == number of times to print, leave
	movl	$SYS_WRITE, %eax	# write()
        syscall                         # print a space
	incl	%r10d			# i++
	jmp	start_loop2

start_loop2_exit:
	subl	$0x31, %r9d		# r9 - 48
	movl	$0x0, %edx		# clear upper register
	movl	%r9d, %eax		# r9d in lower register
	imul	$-0x1, %eax		# r9d * -1 for easy division
	movl	$0x3, %esi
	divl	%esi			# r9 = r9 / -3 to get OG buffer length
	movl	%eax, %r9d		# OG input length in r9 
	movl	$0x0, %r10d		# i = 0

start_loop3:
	leaq    buffer(%rip), %r11      # move address of buffer into r11
        addl    %r10d, %r11d            # buffer address + i
        movb    (%r11), %dil            # 1 byte of buffer[i] is 1st arg
        call    _char                   # perform hex calc and print
	
	incl	%r10d			# i++
	cmpl	%r9d, %r10d		# compare input length with i
	je	start_loop3_exit

	jmp	start_loop3

start_loop3_exit:
	movl	$lengthn, %edx
	movl	$SYS_WRITE, %eax
	movl	$STDOUT_FILENO, %edi
	leal	newline(%rip), %esi
	syscall

	jmp	read_loop		# If all g, then read the next 16 bytes

start_exit:
	movl    $SYS_EXIT, %eax         # exit() system call
        movl    $EXIT_SUCCESS, %edi     # exit status to return to shell
        syscall

start_error:
	movl    $SYS_EXIT, %eax         # exit() system call
        movl    $EXIT_FAILURE, %edi     # exit status to return to shell
        syscall


	.data
buffer:
	.space	16
lengthb = . - buffer

one_char:
	.space	1
lengtho = . - one_char

colon:
	.ascii	": "
lengthc = . - colon

newline:
	.ascii	"\n"
lengthn = . - newline

space:
	.ascii	" "
lengths = . - space

dot:
	.ascii	"."
lengthd = . - dot
