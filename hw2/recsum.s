SYS_PRINT_INT = 1
SYS_PRINT_STR = 4
SYS_READ_INT = 5

	.text
main:
	addiu	$sp, $sp, -8
	sw	$ra, 0($sp)
	sw	$ra, 4($sp)

	li	$v0, SYS_READ_INT
	syscall

	blt	$v0, $zero, error

	move	$a0, $v0
	jal	recsum

	move	$a0, $v0
	li	$v0, SYS_PRINT_INT
	syscall

	lw	$s0, 4($sp)
	lw	$ra, 0($sp)
	addiu	$sp, $sp, 8

	jr	$ra

error:
	li	$v0, 1
	li	$v0, 17
	syscall

recsum:
	addiu   $sp, $sp, -8
        sw      $ra, 0($sp)
        sw      $s0, 4($sp)

	beq	$a0, $zero, base	#Base case if n is 0

	move	$s0, $a0		#A safe copy of n

	addi	$a0, $s0, -1		#Compute n-1 for the recursive call

	jal	recsum

	add	$v0, $v0, $s0		#Compute the result of recursion with the OG n

	lw      $s0, 4($sp)
        lw      $ra, 0($sp)
        addiu   $sp, $sp, 8

	jr	$ra

base:
	li	$v0, 0

	lw      $s0, 4($sp)
        lw      $ra, 0($sp)
        addiu   $sp, $sp, 8

	jr	$ra

	.data
lf:
	.asciiz	"\n"
