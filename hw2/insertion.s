#Alice Yang
#ayang36@jhu.edu
#insertion.s

sys_print_int = 1
sys_print_string = 4
sys_read_int = 5

SLOTS = 128				# at most 128 values
BYTES = 512				# need 128*4 bytes of space

	.text
	.globl	main
main:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	li	$a0, SLOTS
	la	$a1, buffer
	jal	read_array
	ble	$v0, $zero, exit
	sw	$v0, used

	lw	$a0, used
	la	$a1, buffer
	jal	insertion_sort

	lw	$a0, used
	la	$a1, buffer
	jal	print_array
exit:
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4

	jr	$ra

# length = read_array(size, address)
# $v0                 $a0   $a1
read_array:
	addi	$sp, $sp, -16		# save used registers
	sw	$s2, 12($sp)
	sw	$s1, 8($sp)
	sw	$s0, 4($sp)
	sw	$ra, 0($sp)

	move	$s0, $zero		# slots used
	move	$s1, $a1		# address in array
	move	$s2, $a0		# slots available
ra_loop:
	li	$v0, sys_read_int	# read next value
	syscall

	blt	$v0, $zero, ra_done	# break if we read < 0

	sw	$v0, ($s1)		# store value in array
	addi	$s0, $s0, 1		# used++
	addi	$s1, $s1, 4		# address++

	blt	$s0, $s2, ra_loop	# while used < available
ra_done:
	move	$v0, $s0		# return slots used

	lw	$ra, 0($sp)		# restore used registers
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	addi	$sp, $sp, 16

	jr	$ra			# return

# insertion_sort(size, address)
#                $a0   $a1
insertion_sort:
	addi    $sp, $sp, -24           # save used registers
	sw	$s4, 20($sp)
	sw	$s3, 16($sp)
        sw      $s2, 12($sp)
        sw      $s1, 8($sp)
        sw      $s0, 4($sp)
        sw      $ra, 0($sp)
	
	move	$s1, $a1		#s1 = array[0]
	move	$s0, $a0		#s0 = size
	li	$s2, 1			#s2 = i = 1

out_loop:
	li	$t0, 4			#t0 = 4
	mul	$t1, $s2, $t0		#t1 = i * 4 (for address access)
	add	$t2, $s1, $t1		#Address of i
	lw	$t3, 0($t2)		#t3 = array[i]

	sub	$s3, $s2, 1		#s3 = j = i - 1

inn_loop:
	blt	$s3, $zero, end_inn	#While (j >= 0)
	li	$t0, 4			#t0 = 4
	mul	$t1, $s3, $t0		#t1 = j * 4 (for address access)
	add	$t2, $s1, $t1		#Address of j
	lw	$t4, 0($t2)		#t4 = array[j]

	bge	$t3, $t4, end_inn	#While (array[i] < array[j])

        add     $s4, $s3, 1             #s4 = k = j + 1
        li      $t0, 4                  #t0 = 4
        mul     $t1, $s4, $t0           #t1 = k * 4
        add     $s4, $s1, $t1           #Address of k
	sw	$t4, 0($s4)		#Array[k] = Array[j]

	sub	$s3, $s3, 1		#j = j - 1
	j	inn_loop		#Repeat the inner loop
end_inn:
	add	$s4, $s3, 1		#s4 = k = j + 1
	li	$t0, 4			#t0 = 4
	mul	$t1, $s4, $t0		#t1 = k * 4
	add	$s4, $s1, $t1		#Address of j
	sw	$t3, 0($s4)		#Array[k] = Array[i]
	
	add	$s2, $s2, 1		#i = i + 1
	beq	$s2, $s0, end_out	#While (i < size)
	j	out_loop

end_out:
	lw      $ra, 0($sp)             # restore used registers
        lw      $s0, 4($sp)
        lw      $s1, 8($sp)
        lw      $s2, 12($sp)
	lw	$s3, 16($sp)
	lw	$s4, 20($sp)
        addi    $sp, $sp, 24	

	jr	$ra			# return

# print_array(size, address)
#             $a0   $a1
print_array:
	addi	$sp, $sp, -16		# save used registers
	sw	$s2, 12($sp)
	sw	$s1, 8($sp)
	sw	$s0, 4($sp)
	sw	$ra, 0($sp)

	move	$s0, $zero		# current slot
	move	$s1, $a1		# address in array
	move	$s2, $a0		# slots used
pa_loop:
	bge	$s0, $s2, pa_done	# while current < used

	lw	$a0, ($s1)		# load value in array
	jal	print_int		# and print it

	addi	$s0, $s0, 1		# current++
	addi	$s1, $s1, 4		# address++

	b	pa_loop
pa_done:
	lw	$ra, 0($sp)		# restore used registers
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	addi	$sp, $sp, 16

	jr	$ra			# return

# print_int(value)
#           $a0
print_int:
	li	$v0, sys_print_int
	syscall

	la	$a0, lf
	li	$v0, sys_print_string
	syscall

	jr	$ra

# print_string(string)
#              $a0
print_string:
	li	$v0, sys_print_string
	syscall

	la	$a0, lf
	li	$v0, sys_print_string
	syscall

	jr	$ra

	.data
buffer:
	.space	BYTES			# array of 4-byte integers
used:
	.word	0			# used slots in array
lf:
	.asciiz	"\n"

	.end
