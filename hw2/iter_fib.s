#Alice Yang
#ayang36@jhu.edu
#iter_fib.s

print_int = 1
print_str = 4
read_int = 5

	.text

main:
	addi	$sp, $sp, -4
	sw	$ra, 4($sp)

	li	$v0, read_int		#Get integer from stdin
	syscall

	blt	$v0, $zero, error_exit	#Don't accept input .lt. 0

	move	$a0, $v0		
	jal	fibonacci		#Call the fibonacci function

	move	$a0, $v0
	li	$v0, print_int		#Print the fibonacci number
	syscall

	la	$a0, new_line
	li	$v0, print_str
	syscall

exit:
	lw      $ra, 4($sp)
        addi    $sp, $sp, 8

        jr      $ra

error_exit:
	la	$a0, error
	li	$v0, print_str
	syscall

	j	exit

# fibo_num = fibonacci(input)
# $v0		       $a0
fibonacci:
	addi    $sp, $sp, -20           # save used registers
        sw      $s3, 16($sp)
        sw      $s2, 12($sp)
        sw      $s1, 8($sp)
        sw      $s0, 4($sp)
        sw      $ra, 0($sp)

	beq	$a0, $zero, zero_fibo	#If input was 0, fibonacci is 0
	beq	$a0, 1, one_fibo	#If input was 1, fibonacci is 1

	li	$s0, 2			#s0 = n = 2
	li	$s1, 0			#F(0) = 0. Works as F(n - 2)
	li	$s2, 1			#F(1) = 1. Works as F(n - 1)

loop:
	add	$s3, $s1, $s2		#s3 = F(n) = F(n-2) + F(n-1)
	move	$v0, $s3

	add	$s0, $s0, 1		#n = n + 1

	bgt	$s0, $a0, exit_fibo	#Continue if i .le. n

	move	$s1, $s2		#F(n-2) = F(n-1)
	move	$s2, $s3		#F(n-1) = F(n)

	j	loop

exit_fibo:
	lw      $ra, 0($sp)             # restore used registers
        lw      $s0, 4($sp)
        lw      $s1, 8($sp)
        lw      $s2, 12($sp)
        lw      $s3, 16($sp)
        addi    $sp, $sp, 20

	jr	$ra

zero_fibo:
	li	$v0, 0			#a0 = 0
	j	exit_fibo

one_fibo:
	li	$v0, 1			#a0 = 1
	j	exit_fibo


	.data

new_line:
	.asciiz	"\n"

error:
	.asciiz	"Input value invalid.\n"
