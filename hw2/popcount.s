#Alice Yang
#ayang36@jhu.edu
#popcount.s

#The program reads in the second argument in the command line as a string
#Then it converts the string into an int.
#Each character is popped off the string one at a time,
#The ascii values are subtracted by the ascii offset to change them into ints
#Each digit is then individually added back into a new int variable.
#Once the string has been converted to an int, the program counts the set bits
#Check for a set bit in the lowest bit.
#If there is a 1, add it to the overall sum of set bits
#Then shift the int 1 bit to the right
#Continue until the int is 0.

print_int = 1
print_str = 4

	.text
	.globl	popcount

main:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	lw	$a0, 4($a1)		#Read in the first argument
	jal	dec_convert		#Convert string to int

	move	$a0, $v0
	jal	popcount		#Pop and count the bits

	move	$a0, $v0		#Print the number of bits
	li	$v0, print_int
	syscall

	la	$a0, newline		#Print '\n'
	li	$v0, print_str
	syscall

exit:
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4

	jr	$ra

exit_error:
	la	$a0, error		#Print error message
	li	$v0, print_str
	syscall

	j	exit

# Integer = dec_convert(String)
# $v0		        #a0
dec_convert:
	move	$t0, $a0		#A copy of the input
	li	$v0, 0			#The accumulator

dec_convert_loop:
	lb	$t1, 0($t0)		#Load a character
	beq	$t1, $zero, dec_convert_exit	#Exit if it's null

	mul	$v0, $v0, 10		#Multiply accumulator by 10

	addi	$t1, $t1, -48		#ASCII shift from char into int

	blt	$t1, 0, exit_error	#Error handling for invalid input
	bgt	$t1, 9, exit_error

	add	$v0, $v0, $t1		#Add to the accumulator

	addi	$t0, $t0, 1		#Increment the address pointed to

	b	dec_convert_loop

dec_convert_exit:
	#bgt	$v0, 4294967295, exit_error	#Greater than 32-bits

	jr	$ra			#Return

# set_ints = popcount(integer)
# #v0		      $a0
popcount:
	addi	$sp, $sp, -8
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)

	move	$t0, $a0		#A copy of the integer
	
	li	$s0, 0			#Accumulator

popcount_loop:
	beq	$t0, $zero, popcount_exit	#When the number reaches 0, exit

	andi	$t1, $t0, 1		#GOOD Logical and with 1

	add	$s0, $s0, $t1		#If there's a 1, add to accumulator

	srl	$t0, $t0, 1		#Shift the number to the right

	j	popcount_loop

popcount_exit:
	move	$v0, $s0
	
	lw	$s0, 4($sp)
	lw	$ra, 0($sp)
	addi	$sp, $sp, 8

	jr	$ra

	.data

newline:
	.asciiz	"\n"

error:
	.asciiz	"Invalid Input.\n"
