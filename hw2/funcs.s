print_str = 4
print_int = 1

	.text
main:
	#move	$t0, $ra		#BUt $t0 can be changed at any time
	#move	$s0, $ra		#Not good, cos $s- registers have to be saved
	addiu	$sp, $sp, -4		# \ "push" onto the stack
	sw	$ra, 0($sp)		# /

	li	$a0, 10
	li	$a1, 7
	jal	sillyadd
# --->> Puts the address of $ra into here without pushing and popping the stack
	move	$a0, $v0
	li	$v0, print_int		#Print with syscall
	syscall
	
	la	$a0, lf
	li	$v0, print_str
	syscall

	lw	$ra, 0($sp)		#Pop out of the stack
	addiu	$sp, $sp, 4		#4 bytes cos a word is 4 bytes

	jr	$ra

# sum = silladd(a, b)
# $v0		$a0 $a1
sillyadd:
	#Don't need to push and pop for ra because there are no other calls for ra
	add	$v0, $a0, $a1

	jr	$ra


	.data

boom:
	.asciiz	"Boom!"

lf:
	.asciiz	"\n"
