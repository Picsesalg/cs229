print_str = 4
print_int = 1

	.text
main:
	li	$t0, 10

loop:
	beq	$t0, 0, exit	#Looks at value in t0. If it's 0, then go to exit
	
	move	$a0, $t0
	li	$v0, print_int
	syscall
	
	la	$a0, lf
	li	$v0, print_str
	syscall

	sub	$t0, $t0, 1

	j 	loop

exit:
	la 	$a0, boom
	li	$v0, print_str
	syscall
	
	la	$a0, lf
	li	$v0, print_str
	syscall

	jr	$ra

	.data

boom:
	.asciiz	"Boom!"

lf:
	.asciiz	"\n"
