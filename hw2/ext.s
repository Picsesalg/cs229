print_str = 4
print_int = 1

#Increment the wrong amount on the stack
	.text
main:
	#move	$t0, $ra		#BUt $t0 can be changed at any time
	#move	$s0, $ra		#Not good, cos $s- registers have to be saved
	lw	$t0, one		#Prints -1
					#Can't lw two cos it's not a whole word

	move	$a0, $t0

	li	$v0, print_int		#Print with syscall
	syscall
	
	la	$a0, lf
	li	$v0, print_str
	syscall

	lh	$t0, two		#Have to use lh for half word
					#$t0 is 32 bit register,
					#lh is a 16 bit instruction

	move	$a0, $t0
	li	$v0, print_int
	syscall

	la	$a0, lf
	li	$v0, print_int
	syscall

	jr	$ra


	.data
one:	
	.word	-1			#Aligned on a 4 byte boundary
two:
	.half	-2			#Aligned on a 4 byte boundary because the top was 4
three:
	.byte	-3			#Aligned on a 2 byte boundary but it's fine
lf:
	.asciiz	"\n"


