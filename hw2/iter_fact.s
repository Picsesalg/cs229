# Starter code for the iterative factorial function.
# Your code should certainly have more comments that
# explain what's going on.

print_int = 1
print_string = 4
read_int = 5

	.text
# Note that for main() we make no assumption about the
# caller having set aside memory for saving arguments.
# Luckily, in this case, we don't need to save them.
main:
	addi	$sp, $sp, -8
	sw	$ra, 4($sp)

	li	$v0, read_int
	syscall

	move	$a0, $v0
	jal	factorial

	move	$a0, $v0
	li	$v0, print_int
	syscall

	la	$a0, lf
	li	$v0, print_string
	syscall

	lw	$ra, 4($sp)
	addi	$sp, $sp, 8

	jr	$ra

#
factorial:
	li	$v0, 1
loop:
	ble	$a0, 1, done

	mul	$v0, $v0, $a0
	addi	$a0, $a0, -1

	j	loop
done:
	jr	$ra

	.data
lf:
	.asciiz	"\n"
	.end
