#Alice Yang
#ayang36@jhu.edu
#delay.s

print_int = 1
print_str = 4

	.text

main:

	addi	$sp, $sp, -8
	sw	$ra, 4($sp)

	lui	$a0, 4096		#Load 111
	addi	$t9, $zero, 2
	lw	$t0, 0($a0)		#0x0000000 (0)
	addi	$t9, $zero, 2

	lui	$a0, 4096		#Load 222
	addi	$t9, $zero, 1
	lw	$t1, 4($a0)
	addi	$t9, $zero, 2		

	add	$t2, $t0, $t1		#111 + 222

	addu	$a0, $zero, $t2		#Move 333 to a0

	jal	print
	addi	$t3, $zero, 1

	lw	$ra, 4($sp)
	addi	$sp, $sp, 8

	jr	$ra
	addi	$t0, 1

print:
	ori	$v0, $zero, print_int	#Load v0 for print
	syscall

	lui	$s0, 4096
	ori	$a0, $s0, 8
	ori	$v0, $zero, print_str
	syscall
	
	jr	$ra
	addi	$t0, $zero, 1

	.data
one:
	.word	111
two:
	.word	222
lf:
	.asciiz	"\n"
