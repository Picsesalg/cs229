#Alice Yang
#ayang36@jhu.edu
#isqrt.s

sys_print_int = 1
sys_print_str = 4
sys_read_int = 5

	.text
main:
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)

	li	$v0, sys_read_int	#Reads in an int from stdin
	syscall
	
	move	$a0, $v0

	blt	$a0, $zero, exit_error	#Reject integers .lt. 0
	
				
	jal	sqrt

	move	$a0, $v0
	li	$v0, sys_print_int
	syscall

	la	$a0, new_line
	li	$v0, sys_print_str
	syscall

exit:
	lw      $ra, 0($sp)
        addi    $sp, $sp, 4
	
	jr	$ra

exit_error:
	move	$a0, $zero
	li	$v0, sys_print_int
	syscall

	la	$a0, new_line
	li	$v0, sys_print_str
	syscall

	j	exit

# root = sqrt(int n)
# $v0         $a0
#Using Newton's method
sqrt:
	addi    $sp, $sp, -32           # save used registers
        sw	$s6, 28($sp)
	sw	$s5, 24($sp)
	sw	$s4, 20($sp)
	sw	$s3, 16($sp)
	sw      $s2, 12($sp)
        sw      $s1, 8($sp)
        sw      $s0, 4($sp)
        sw      $ra, 0($sp)

	move	$s0, $a0		#s0 = x = n
	li	$t0, 1			#t0 = y = 1
	li	$t1, 2

loop:
	ble	$s0, $t0, end_loop	#WHile(x>y)

	add	$s0, $s0, $t0		#x = x+y
	div	$s0, $t1		#x = x/2
	mflo	$s0

	div	$a0, $s0		#y = y/2
	mflo	$t0

	j	loop

end_loop:
	move	$v0, $s0

	lw      $ra, 0($sp)             # restore used registers
        lw      $s0, 4($sp)
        lw      $s1, 8($sp)
        lw      $s2, 12($sp)
	lw	$s3, 16($sp)
	lw	$s4, 20($sp)
	lw	$s5, 24($sp)
	lw	$s6, 28($sp)
        addi    $sp, $sp, 32

	jr	$ra
	
	.data
error:
	.asciiz "Entered value invalid.\n"

new_line:
	.asciiz	"\n"
