# Starter code for the recursive factorial function.
# Your code should certainly have more comments that
# explain what's going on.

print_int = 1
print_string = 4
read_int = 5

	.text
# Note that for main() we make no assumption about the
# caller having set aside memory for saving arguments.
# Luckily, in this case, we don't need to save them.
main:
	addi	$sp, $sp, -8
	sw	$ra, 4($sp)

	li	$v0, read_int
	syscall

	move	$a0, $v0
	jal	factorial

	move	$a0, $v0
	li	$v0, print_int
	syscall

	la	$a0, lf
	li	$v0, print_string
	syscall

	lw	$ra, 4($sp)
	addi	$sp, $sp, 8

	jr	$ra

# This version uses the memory the caller set aside
# to save $a0 across the recursive calls.
factorial:
	addi	$sp, $sp, -8
	sw	$ra, 4($sp)

	sw	$a0, 8($sp)

	ble	$a0, 1, base

	addi	$a0, $a0, -1
	jal	factorial
	lw	$t0, 8($sp)
	mul	$v0, $v0, $t0

	b	done
base:
	li	$v0, 1
done:
	lw	$ra, 4($sp)
	addi	$sp, $sp, 8

	jr	$ra

	.data
lf:
	.asciiz	"\n"
	.end
