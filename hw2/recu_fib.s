#Alice Yang
#ayang36@jhu.edu
#recu_fib.s

print_int = 1
print_str = 4
read_int = 5
	
	.text

main:
	addi    $sp, $sp, -8
        sw      $ra, 4($sp)

	li	$v0, read_int		#Read the user input
	syscall

	blt	$v0, $zero, error_exit	#Continue with input >= 0

	move	$a0, $v0
	jal	fibonacci		#Call the fibonacci function

	move	$a0, $v0		#Print the result
	li	$v0, print_int
	syscall

	la	$a0, new_line
	li	$v0, print_str
	syscall

exit:
	lw      $ra, 4($sp)
        addi    $sp, $sp, 8

	jr	$ra

error_exit:
	la	$a0, error
	li	$v0, print_str
	syscall

	j	exit

# fibo_num = fibonacci(input)
# $v0		       #a0
fibonacci:
	addiu	$sp, $sp, -12
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)

	ble	$a0, 1, base		#Continue if n > 1

	move	$s0, $a0		#Safe copy of n

	addi	$a0, $s0, -1		#(n-1)
	jal	fibonacci		#F(n-1)
	move	$s1, $v0		#Store F(n-1)

	addi	$a0, $s0, -2		#(n-2)
	jal	fibonacci		#F(n-2)

	add	$v0, $s1, $v0		#F(n-1) + F(n-2)

	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	addiu	$sp, $sp, 12

	jr	$ra
base:
	move	$v0, $a0

end_fibonacci:
	lw	$ra, 0($sp)
        lw      $s0, 4($sp)
	lw	$s1, 8($sp)
        addiu   $sp, $sp, 12

	jr	$ra

	.data

error:
	.asciiz	"Input value is invalid.\n"

new_line:
	.asciiz	"\n"
